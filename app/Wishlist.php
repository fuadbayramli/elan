<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
  protected $fillable = [
    'user_id',
    'sticker_id',
  ];

  public function user(){
     return $this->belongsTo(User::class);
  }

  public function sticker(){
     return $this->belongsTo(Sticker::class);
  }
}
