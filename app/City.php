<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
      'name',
      'slug'
    ];

    public function stickers(){
      return $this->hasMany('App\Sticker', 'city_id');
    }
}
