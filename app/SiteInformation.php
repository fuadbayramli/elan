<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteInformation extends Model
{
    protected $table = 'site_information';
    protected $fillable = [
        'name',
        'main_email',
        'main_number',
        'address',
        'link',
        'logo',
        'privacy_policy',
    ];
}
