<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StickerImage extends Model
{
  protected $fillable = ['sticker_id', 'file_name'];

  public function sticker()
  {
    return $this->belongsTo('App\Sticker');
  }


}
