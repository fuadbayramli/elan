<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sticker extends Model

{
    protected $fillable = [
      'name',
      'city_id',
      'description',
      'img',
      'author_name',
      'author_email',
      'phone',
      'price',
      'new',
      'user_id',
      'sub_category_id',
      'status',
    ];


    public function city(){
      return $this->belongsTo('App\City');
    }

    public function user(){
      return $this->belongsTo('App\User');
    }

    public function sub_category(){
      return $this->belongsTo('App\SubCategory');
    }

    public function images(){
      return $this->hasMany('App\StickerImage', 'sticker_id');
    }

    public function wishlist(){
     return $this->hasMany(Wishlist::class);
    }


}
