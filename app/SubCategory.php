<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
  protected $fillable = [
    'category_id',
    'name',
    'slug',
  ];

  public function category(){
    return $this->belongsTo('App\Category');
  }

  public function stickers(){
    return $this->hasMany('App\Sticker','sub_category_id')->where('status', 1);
  }

}
