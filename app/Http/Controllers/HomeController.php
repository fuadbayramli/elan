<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Sticker;
use App\Category;
use App\City;
use App\SubCategory;
class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */


  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index(Request $request)
  {
    $_stickers = Sticker::where('status', 1);

    $latest_stickers = Cache::remember('latest_stickers', 86400, function () {
      return Sticker::where('status', 1)->orderBy('created_at','desc')->take(8)->get();
    });

    $categories = Cache::remember('categories_index', 86400, function () {
      return Category::select('name','id')->with('sub_categories')->get();
    });

    $cities = Cache::remember('cities_index', 86400, function () {
      return City::orderBy('name', 'ASC')->select('name','slug','id')->get();
    });

    #Generate Cities Table
    $mk_column1 = NULL;
    $mk_column2 = NULL;
    $mk_column3 = NULL;
    $mk_column4 = NULL;
    $mk_column5 = NULL;
    $mk_column6 = NULL;
    $mk_columnRows = round(count($cities)/6);

    foreach ($cities as $key => $value){
      if($key > 0 && $key <= $mk_columnRows){
        $mk_column1.='<div><a class="text-dark city btn" href="'.url('/city').'/'.$value->slug.'">'.$value->name.'</a></div>';
      }elseif($key > $mk_columnRows && $key <= $mk_columnRows*2){
        $mk_column2.='<div><a class="text-dark city btn" href="'.url('/city').'/'.$value->slug.'">'.$value->name.'</a></div>';
      }elseif($key > $mk_columnRows*2 && $key <= $mk_columnRows*3){
        $mk_column3.='<div><a class="text-dark city btn" href="'.url('/city').'/'.$value->slug.'">'.$value->name.'</a></div>';
      }elseif($key > $mk_columnRows*3 && $key <= $mk_columnRows*4){
        $mk_column4.='<div><a class="text-dark city btn" href="'.url('/city').'/'.$value->slug.'">'.$value->name.'</a></div>';
      }elseif($key > $mk_columnRows*4 && $key <= $mk_columnRows*5){
        $mk_column5.='<div><a class="text-dark city btn" href="'.url('/city').'/'.$value->slug.'">'.$value->name.'</a></div>';
      }else{
        $mk_column6.='<div><a class="text-dark city btn" href="'.url('/city').'/'.$value->slug.'">'.$value->name.'</a></div>';
      }
    }

    if ($request->has('sub_category')) {
      if(!empty($request->get('sub_category'))){
        $_stickers->where('sub_category_id', $request->get('sub_category'));
      }
    }

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $_stickers->where('name', 'LIKE', "%{$request->get('q')}%");
      }else{

      }
    }

    if(request()->get('sort')){
      if(request()->get('sort') == 0){
        $_stickers->orderBy('id','desc');
      }elseif(request()->get('sort') == 1){
        $_stickers->orderBy('price', 'asc');
      }elseif(request()->get('sort') == 2){
        $_stickers->orderBy('price', 'desc');
      }

      $stickers = $_stickers->with('city')->paginate(8);
      $stickers->appends(request()->query());

    }else{
      $_stickers->orderBy('id','desc');
      $stickers = $_stickers->with('city')->paginate(8);
    }





    if($request->ajax()){

      return view('site.stickers.ajax.sticker',compact('stickers'));

    }else{

      return view('home',
      compact(
        'stickers','categories','mk_column1','mk_column2','latest_stickers',
        'mk_column3','mk_column4','mk_column5','mk_column6'
        )
      );
    }



  }

  public function stickersWithSubCategorySlug(Request $request,$base_slug,$slug){

    $categories = Cache::remember('categories_sub_category_stickers', 86400, function () {
      return Category::with('sub_categories')->get();
    });

    $sub_category = Cache::remember("sub_categories_{$slug}", 86400, function () use ($slug){
      return SubCategory::where('slug', $slug)
      ->firstOrFail();
    });



    $title = $sub_category->name;

    $_stickers = Sticker::where('sub_category_id', $sub_category->id)
    ->where('status', 1);

    if ($request->has('sub_category')) {
      if(!empty($request->get('sub_category'))){
        $_stickers->where('sub_category_id', $request->get('sub_category'));
      }
    }

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $_stickers->where('name', 'LIKE', "%{$request->get('q')}%");
      }
    }

    if(request()->get('sort')){
      if(request()->get('sort') == 0){
        $_stickers->orderBy('id','desc');
      }elseif(request()->get('sort') == 1){
        $_stickers->orderBy('price', 'asc');
      }elseif(request()->get('sort') == 2){
        $_stickers->orderBy('price', 'desc');
      }

      $stickers = $_stickers->with('city')->paginate(8);
      $stickers->appends(request()->query());

    }else{
      $_stickers->orderBy('id','desc');
      $stickers = $_stickers->with('city')->paginate(8);
    }

    if($request->ajax()){
      return view('site.stickers.ajax.sticker',compact('stickers'));
    }else{
      return view('site.stickers.cat-stickers', compact('sub_category','title','stickers','categories'));
    }
  }

  public function stickersWithCitySlug(Request $request,$slug){

    $categories = Cache::remember('categories_city_stickers', 86400, function () {
      return Category::with('sub_categories')->get();
    });

    $city = Cache::remember("cities_{$slug}", 86400, function () use ($slug){
      return City::where('slug', $slug)
      ->firstOrFail();
    });

    $title = $city->name;
    $_stickers = Sticker::where('city_id', $city->id)
    ->where('status', 1);

    if ($request->has('sub_category')) {
      if(!empty($request->get('sub_category'))){
        $_stickers->where('sub_category_id', $request->get('sub_category'));
      }
    }

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $_stickers->where('name', 'LIKE', "%{$request->get('q')}%");
      }else{

      }
    }

    if(request()->get('sort')){
      if(request()->get('sort') == 0){
        $_stickers->orderBy('id','desc');
      }elseif(request()->get('sort') == 1){
        $_stickers->orderBy('price', 'asc');
      }elseif(request()->get('sort') == 2){
        $_stickers->orderBy('price', 'desc');
      }

      $stickers = $_stickers->with('city')->paginate(8);
      $stickers->appends(request()->query());

    }else{
      $_stickers->orderBy('id','desc');
      $stickers = $_stickers->with('city')->paginate(8);
    }


    if($request->ajax()){

      return view('site.stickers.ajax.sticker',compact('stickers'));

    }else{
      return view('site.stickers.cat-stickers', compact('city','title','stickers','categories'));
    }
  }



  public function about(){
    return view('site.other-pages.about');
  }


  public function searchJson(Request $request){
    $result = Sticker::where('name', 'LIKE', "%{$request->q}%")->orderBy('created_at','desc')->get()->take(3);
    return response()->json($result);
  }





}
