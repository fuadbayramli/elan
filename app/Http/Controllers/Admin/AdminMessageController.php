<?php

namespace App\Http\Controllers\Admin;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller as Controller;


class AdminMessageController extends Controller
{


  public function user_access($page_slug){
    $return_val = false;
    if(Auth::check()){
      $permissions = array();
      foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
        array_push($permissions,$rol_item);
      }
      if (in_array($page_slug,$permissions)){
        $return_val = true;
      }
    }
    if(!$return_val){
      return abort(403);
    }
  }



  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $this->user_access('index');

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $messages = Message::where('name', 'LIKE', "%{$request->get('q')}%")
        ->orWhere('email','LIKE', "%{$request->get('q')}%")
        ->orWhere('phone','LIKE', "%{$request->get('q')}%")
        ->orWhere('message','LIKE', "%{$request->get('q')}%")
        ->orderBy('created_at', 'desc')
        ->paginate(50);
      }else{
        $messages = Message::orderBy('created_at', 'desc')->paginate(50);
      }
    }else{
      $messages = Message::orderBy('created_at', 'desc')->paginate(50);
    }

    return view('admin.messages.index', compact('messages'));
  }


  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {

  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {

  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {

  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $this->user_access('delete');

    $order = Message::findOrFail($id);
    $order->delete();

    return redirect('/admin/messages');
  }

  public function deleteMessage(Request $request){
    $messages = Message::findOrFail($request->m_id);
    foreach($messages as $message){
      $delete = $message->delete();
    }

    if($delete){
      return response()->json("success");
    }else{
      return response()->json("error");
    }
  }
}
