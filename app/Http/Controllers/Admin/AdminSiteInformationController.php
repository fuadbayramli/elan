<?php

namespace App\Http\Controllers\Admin;

// use App\Http\Requests\SiteInformationCreateRequest;
// use App\Http\Requests\SiteInformationEditRequest;
use App\SiteInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller as Controller;

class AdminSiteInformationController extends Controller
{

  public function user_access($page_slug){
    $return_val = false;
    if(Auth::check()){
      $permissions = array();
      foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
        array_push($permissions,$rol_item);
      }
      if (in_array($page_slug,$permissions)){
        $return_val = true;
      }
    }
    if(!$return_val){
      return abort(403);
    }
  }



  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $this->user_access('site-informations-manage');

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $site_informations = SiteInformation::where('name', 'LIKE', "%{$request->get('q')}%")
        ->orWhere('main_email','LIKE', "%{$request->get('q')}%")
        ->orWhere('main_number','LIKE', "%{$request->get('q')}%")
        ->orWhere('address','LIKE', "%{$request->get('q')}%")
        ->orWhere('link','LIKE', "%{$request->get('q')}%")
        ->orderBy('created_at', 'desc')
        ->paginate(50);
      }else{
        $site_informations = SiteInformation::orderBy('created_at', 'desc')->paginate(50);
      }
    }else{
      $site_informations = SiteInformation::orderBy('created_at', 'desc')->paginate(50);
    }

    return view('admin.site-information.index', compact( 'site_informations'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $this->user_access('site-informations-manage');

    return view('admin/site-information/create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $logo = Input::file('logo');
    $filename = time() . '.' . $logo->getClientOriginalExtension();
    $path = public_path('images/' . $filename);

    Image::make($logo->getRealPath())->fit(200, 44)->save($path);

    $site_information = new SiteInformation;
    $site_information->name = $request->name;
    $site_information->main_email = $request->main_email;
    $site_information->main_number = $request->main_number;
    $site_information->address = $request->address;
    $site_information->link = $request->link;
    $site_information->privacy_policy = $request->privacy_policy;
    $site_information->logo = $filename;

    $site_information->save();

    return redirect('admin/site-information');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $this->user_access('site-informations-manage');

    $site_information = SiteInformation::findOrFail($id);
    return view('admin.site-information.edit',compact('site_information'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $logo = Input::file('logo');

    $site_information = SiteInformation::find($id);
    $site_information->name = $request->name;
    $site_information->main_email = $request->main_email;
    $site_information->main_number = $request->main_number;
    $site_information->address = $request->address;
    $site_information->privacy_policy = $request->privacy_policy;
    $site_information->link = $request->link;

    if ($logo != null) {

      $file= $site_information->logo;
      $old_file_name = public_path().'/images/'.$file;
      File::delete($old_file_name);

      $filename = time() . '.' . $logo->getClientOriginalExtension();

      $path = public_path('images/' . $filename);
      Image::make($logo->getRealPath())->fit(200, 44)->save($path);
      $site_information->logo = $filename;
    }

    $site_information->save();

    return redirect('admin/site-information');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $this->user_access('site-informations-manage');

    $site_information = SiteInformation::findOrFail($id);
    $file= $site_information->logo;
    $filename = public_path().'/images/'.$file;
    File::delete($filename);

    $site_information->delete();

    return redirect('/admin/site-information');
  }
}
