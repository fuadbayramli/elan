<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller as Controller;

class AdminHomeController extends Controller
{


    public function user_access($page_slug){
        $return_val = false;
        if(Auth::check()){
            $permissions = array();
            foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
                array_push($permissions,$rol_item);
            }
            if (in_array($page_slug,$permissions)){
                $return_val = true;
            }
        }
        if(!$return_val){
            return abort(403);
        }

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $this->user_access('index');

        return view('admin.index');

    }








}
