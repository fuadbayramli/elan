<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RoleCreateRequest;
use App\Http\Requests\RoleEditRequest;
use App\Role;
use App\RoleItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller as Controller;

class AdminRoleController extends Controller
{



    public function user_access($page_slug){
        $return_val = false;
        if(Auth::check()){
            $permissions = array();
            foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
                array_push($permissions,$rol_item);
            }
            if (in_array($page_slug,$permissions)){
                $return_val = true;
            }
        }
        if(!$return_val){
            return abort(403);
        }
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->user_access('index');

        $roles = Role::orderBy('created_at', 'desc')
            ->paginate(50);
        return view('admin.roles.index', compact( 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user_access('create');

        $role_items = RoleItem::pluck('name', 'id')->all();

        return view('admin.roles.create', compact('role_items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleCreateRequest $request)
    {
        $role = new Role;
        $role->name = $request->name;
        $role->slug = $request->slug;

        $role->save();

        $role->role_items()->sync($request->role_items);

        return redirect('admin/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->user_access('update');

        $role = Role::with('role_items')->where('id', $id)->first();

        $role_items = RoleItem::pluck('name', 'id')->all();

        return view('admin.roles.edit', compact('role','role_items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleEditRequest $request, $id)
    {
        $role = Role::find($id);
        $role->name = $request->name;
        $role->slug = $request->slug;
        $role->save();

        $role->role_items()->sync($request->role_items);

        return redirect('admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user_access('delete');

        $role = Role::findOrFail($id);
        $role->delete();

        return redirect('/admin/roles');
    }
}
