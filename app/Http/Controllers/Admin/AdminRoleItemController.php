<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RoleItemCreateRequest;
use App\Http\Requests\RoleItemEditRequest;
use App\RoleItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class AdminRoleItemController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $role_items = RoleItem::where('name', 'LIKE', "%{$request->get('q')}%")
        ->orWhere('slug','LIKE', "%{$request->get('q')}%")
        ->orderBy('created_at', 'desc')
        ->paginate(50);
      }else{
        $role_items = RoleItem::orderBy('created_at', 'desc')->paginate(50);
      }
    }else{
      $role_items = RoleItem::orderBy('created_at', 'desc')->paginate(50);
    }

    return view('admin.role-items.index', compact( 'role_items'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {

  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {

  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {

  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {

  }
}
