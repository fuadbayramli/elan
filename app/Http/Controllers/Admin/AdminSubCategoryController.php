<?php

namespace App\Http\Controllers\Admin;

use App\SubCategory;
use App\Category;
use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller as Controller;

class AdminSubCategoryController extends Controller
{


  public function user_access($page_slug){
    $return_val = false;
    if(Auth::check()){
      $permissions = array();
      foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
        array_push($permissions,$rol_item);
      }
      if (in_array($page_slug,$permissions)){
        $return_val = true;
      }
    }
    if(!$return_val){
      return abort(403);
    }

  }


  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $this->user_access('index');

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $sub_categories = SubCategory::where('name', 'LIKE', "%{$request->get('q')}%")
        ->orderBy('created_at', 'desc')
        ->paginate(50);
      }else{
        $sub_categories = SubCategory::orderBy('created_at', 'desc')->paginate(50);
      }
    }else{
      $sub_categories = SubCategory::orderBy('created_at', 'desc')->paginate(50);
    }

    return view('admin.sub-categories.index', compact( 'sub_categories'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $this->user_access('create');

    $categories = Category::pluck('name', 'id')->all();
    return view('admin/sub-categories/create', compact('categories'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //Validation
    $validator = Validator::make($request->all(), [
      'name' => 'required|min:2|max:255',
      'category_id' => 'required',
    ],
    [
      'name.required' => 'Alt kateqoriyanın adını daxil edin',
      'name.min' => 'Alt kateqoriyanın adı minimum 2 simvol ola bilər',
      'name.max' => 'Alt kateqoriyanın adı maksimum 255 simvol ola bilər',
      'category_id.required' => 'Kateqoriyanı seçin'
    ]
  );

  if($validator->fails()){
    $errors = $validator->errors();
    $categories = Category::pluck('name', 'id')->all();
    return view('admin.sub-categories.create', compact('categories','errors'));
  }

  $sub_category = SubCategory::create([
    'name' => $request->name,
    'slug' => str_slug($request->name),
    'category_id' => $request->category_id
  ]);

  if($sub_category){
    Session::flash('success', 'Yeni alt kateqoriya yaradıldı.');
  }else{
    Session::flash('error', 'Səhv baş verdi.');
  }

  return redirect('admin/sub-categories');
}

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
  //
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
  $this->user_access('update');

  $sub_category = SubCategory::findOrFail($id);
  $categories = Category::pluck('name','id')->all();

  return view('admin.sub-categories.edit',compact('categories', 'sub_category'));
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
  //Validation
  $validator = Validator::make($request->all(), [
    'name' => 'required|min:2|max:255',
    'category_id' => 'required',
  ],
  [
    'name.required' => 'Kateqoriyanın adını daxil edin',
    'name.min' => 'Kateqoriyanın adı minimum 2 simvol ola bilər',
    'name.max' => 'Kateqoriyanın adı maksimum 255 simvol ola bilər',
    'category_id.required' => 'Kateqoriyanı seçin'
  ]
);

if($validator->fails()){
  $errors = $validator->errors();
  $sub_category = SubCategory::findOrFail($id);
  $categories = Category::pluck('name','id')->all();
  return view('admin.sub-categories.edit', compact('categories','errors','sub_category'));
}

#Update sticker
$sub_category = SubCategory::where('id', $id)->update([
  'name'=>$request->name,
  'slug' => str_slug($request->name),
  'category_id' => $request->category_id
]);

if($sub_category){
  Session::flash('success', 'Alt kateqoriya uğurla dəyişdirildi.');
}else{
  Session::flash('error', 'Səhv baş verdi.');
}

return redirect('admin/sub-categories');
}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
  $this->user_access('delete');

  $sub_category = SubCategory::findOrFail($id);
  $sub_category->delete();

  return redirect('/admin/categories');
}
}
