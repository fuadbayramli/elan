<?php

namespace App\Http\Controllers\Admin;
use App\City;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Validator;
use App\Http\Controllers\Controller as Controller;

class AdminCityController extends Controller
{

  public function user_access($page_slug){
    $return_val = false;
    if(Auth::check()){
      $permissions = array();
      foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
        array_push($permissions,$rol_item);
      }
      if (in_array($page_slug,$permissions)){
        $return_val = true;
      }
    }
    if(!$return_val){
      return abort(403);
    }

  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $this->user_access('index');

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $cities = City::where('name', 'LIKE', "%{$request->get('q')}%")
        ->orderBy('created_at', 'desc')
        ->paginate(50);
      }else{
        $cities = City::orderBy('created_at', 'desc')->paginate(50);
      }
    }else{
      $cities = City::orderBy('created_at', 'desc')->paginate(50);
    }

    return view('admin.cities.index', compact( 'cities'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $this->user_access('create');

    return view('admin/cities/create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //Validation
    $validator = Validator::make($request->all(), [
      'name' => 'required|min:2|max:255',
    ],
    [
      'name.required' => 'Elanın adını daxil edin',
      'name.min' => 'Elanın adı minimum 2 simvol ola bilər',
      'name.max' => 'Elanın adı maksimum 255 simvol ola bilər',
    ]
  );

  if($validator->fails()){
    $errors = $validator->errors();
    return view('admin.cities.create', compact('errors'));
  }

  $city = City::create([
    'name' => $request->name,
    'slug' => str_slug($request->name)
  ]);

  if($city){
    Cache::forget('cities');
    Session::flash('success', 'Yeni şəhər yaradıldı.');
  }else{
    Session::flash('error', 'Səhv baş verdi.');
  }

  return redirect('admin/cities');
}

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
  //
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
  $this->user_access('update');

  $city = City::findOrFail($id);
  return view('admin.cities.edit',compact('city'));
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
  //Validation
  $validator = Validator::make($request->all(), [
    'name' => 'required|min:2|max:255',
  ],
  [
    'name.required' => 'Elanın adını daxil edin',
    'name.min' => 'Elanın adı minimum 2 simvol ola bilər',
    'name.max' => 'Elanın adı maksimum 255 simvol ola bilər',
  ]
);

if($validator->fails()){
  $errors = $validator->errors();
  $city = City::findOrFail($id);
  return view('admin.cities.edit', compact('errors','city'));
}

#Update sticker
$city = City::where('id', $id)->update([
  'name'=>$request->name,
  'slug' => str_slug($request->name)
]);

if($city){
  Cache::forget('cities');
  Session::flash('success', 'Şəhər uğurla dəyişdirildi.');
}else{
  Session::flash('error', 'Səhv baş verdi.');
}

return redirect('admin/cities');
}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
  $this->user_access('delete');

  $city = City::findOrFail($id);
  $city->delete();

  return redirect('/admin/cities');
}
}
