<?php

namespace App\Http\Controllers\Admin;
use App\Sticker;
use App\StickerImage;
use App\Category;
use App\SubCategory;
use App\City;
use Illuminate\Http\Request;
use Session;
use Validator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller as Controller;

class AdminStickerController extends Controller
{

  public function user_access($page_slug){
    $return_val = false;
    if(Auth::check()){
      $permissions = array();
      foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
        array_push($permissions,$rol_item);
      }
      if (in_array($page_slug,$permissions)){
        $return_val = true;
      }
    }
    if(!$return_val){
      return abort(403);
    }
  }



  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $this->user_access('index');

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $stickers = Sticker::where('name', 'LIKE', "%{$request->get('q')}%")
        ->orWhere('author_name','LIKE', "%{$request->get('q')}%")
        ->orderBy('created_at', 'desc')
        ->paginate(20);
      }else{
        $stickers = Sticker::orderBy('created_at', 'desc')->paginate(20);
      }
    }else{
      $stickers = Sticker::orderBy('created_at', 'desc')->paginate(20);
    }

    return view('admin.stickers.index', compact( 'stickers'));
  }


  public function stickersWithUserId(Request $request, $id){

    $this->user_access('index');

    $_stickers = Sticker::where('user_id', $id);

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $stickers = $_stickers->where('name', 'LIKE', "%{$request->get('q')}%")->orderBy('created_at', 'desc')->paginate(20);
      }else{
        $stickers = $_stickers->orderBy('created_at', 'desc')->paginate(20);
      }
    }else{
      $stickers = $_stickers->orderBy('created_at', 'desc')->paginate(20);
    }

    return view('admin.stickers.index', compact('stickers'));
  }

  public function stickersWithCityId(Request $request, $id){

    $this->user_access('index');

    $_stickers = Sticker::where('city_id', $id);

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $stickers = $_stickers->where('name', 'LIKE', "%{$request->get('q')}%")->orderBy('created_at', 'desc')->paginate(20);
      }else{
        $stickers = $_stickers->orderBy('created_at', 'desc')->paginate(20);
      }
    }else{
      $stickers = $_stickers->orderBy('created_at', 'desc')->paginate(20);
    }

    return view('admin.stickers.index', compact( 'stickers'));
  }

  public function stickersWithSubCategoryId(Request $request, $base_slug,$slug){

    $this->user_access('index');

    $sub_category = SubCategory::where('slug', $slug)
    ->firstOrFail();
    $_stickers = Sticker::where('sub_category_id', $sub_category->id);

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $stickers = $_stickers->where('name', 'LIKE', "%{$request->get('q')}%")->orderBy('created_at', 'desc')->paginate(20);
      }else{
        $stickers = $_stickers->orderBy('created_at', 'desc')->paginate(20);
      }
    }else{
      $stickers = $_stickers->orderBy('created_at', 'desc')->paginate(20);
    }

    return view('admin.stickers.index', compact( 'stickers'));
  }



  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */


  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */



  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $this->user_access('update');

    $sticker = Sticker::findOrFail($id);

    $categories = Cache::remember('categories', 86400, function () {
      return Category::select('id','name')->get();
    });

    $cities = Cache::remember('cities', 86400, function () {
      return City::select('id','name', 'slug')->get();
    });

    return view('admin.stickers.edit', compact('sticker','categories','cities'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $sticker = Sticker::findOrFail($id);
    //Validation
    $validator = Validator::make($request->all(), [
      'name' => 'required|min:2|max:255',
      'sub_category_id' => 'required',
      'category' => 'required',
      'city_id'=> 'required',
      'description'=> 'required|min:2',
      'author_name'=> 'required',
      'author_email'=> 'required|email',
      'phone'=> 'required|numeric',
      'price'=> 'required|numeric',
      'status' => 'required',
    ],
    [
      'name.required' => 'Elanın adını daxil edin',
      'name.min' => 'Elanın adı minimum 2 simvol ola bilər',
      'name.max' => 'Elanın adı maksimum 255 simvol ola bilər',
      'category.required' => 'Kateqoriyanı daxil edin',
      'sub_category_id.required' => 'Alt kateqoriyanı daxil edin',
      'city_id.required' => 'Şəhəri daxil edin',
      'description.required' => 'Məzmun boş ola bilməz',
      'description.min' => 'Məzmun minimum 2 simvol ola bilər',
      'author_name.required' => 'Müəllif boş ola bilməz',
      'author_email.required' => 'Email boş ola bilməz',
      'author_email.unique' => 'Email artıq istifadə olunub',
      'author_email.email' => 'Emaili düzgün daxil edin',
      'phone.required' => 'Mobil nömrəni daxil edin',
      'phone.numeric' => 'Mobil nömrəni düzgün daxil edin',
      'price.required' => 'Qiyməti daxil edin',
      'price.numeric' => 'Qiyməti düzgün daxil edin',
      'status.required' => 'Statusu seçin',
    ]
  );

  if($validator->fails()){
    $errors = $validator->errors();

    $categories = Cache::remember('categories', 86400, function () {
      return Category::select('id','name')->get();
    });

    $cities = Cache::remember('cities', 86400, function () {
      return City::select('id','name','slug')->get();
    });

    return view('admin.stickers.edit', compact('categories','cities','errors','sticker'));
  }

  #Update sticker
  $edit = Sticker::where('id', $id)->update([
    'name'=>$request->name,
    'sub_category_id'=>$request->sub_category_id,
    'city_id'=>$request->city_id,
    'description'=>$request->description,
    'author_name'=>$request->author_name,
    'author_email'=>$request->author_email,
    'phone'=>$request->phone,
    'price'=>$request->price,
    'new'=>$request->new ? 1 : 0 ?? 0,
    'status'=>$request->status,
    'img' => $request->img,
    'user_id' => Auth::user()->id
  ]);

  if($edit){
    Session::flash('success', 'Elan uğurla dəyişdirildi.');
    Cache::forget('stickers');
  }else{
    Session::flash('error', 'Səhv baş verdi.');
  }

  return redirect('admin/stickers');
}


public function uploadImage(Request $request){

  $validator = Validator::make($request->all(), [
    'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096'
  ]);

  if ($validator->fails()) {
    return response()->json([
      'message' => 'Error'
    ], 400);
  }

  $photos = $request->file('file');

  if (!is_array($photos)) {
    $photos = [$photos];
  }

  for ($i = 0; $i < count($photos); $i++) {
    $photo = $photos[$i];
    $name = sha1(date('YmdHis') . str_random(30));
    $filename = $name . '.' . $photo->getClientOriginalExtension();
    $directory = 'images';
    $full_filename = "$directory/$filename";
    $path = public_path($full_filename);

    Image::make($photo->getRealPath())->fit(310,277)->save($path);

    return response()->json([
      'file_name'=>$full_filename
    ]);
  }

  return response()->json([
    'message' => ['success']
  ], 200);

}
/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
  $sticker = Sticker::findOrFail($id);
  $file = $sticker->img;
  $filename = public_path().'/'.$file;
  File::delete($filename);

  $sticker->delete();
  Cache::forget('latest_stickers');

  return redirect('/admin/stickers');
}

public function deleteImage($image_id){
  $image = StickerImage::findOrFail($image_id);
  $filename = public_path().'/'.$image->file_name;
  File::delete($filename);
  $delete = $image->delete();
  if($delete){
    return response()->json("success");
  }else{
    return response()->json("error");
  }
}

public function deleteStickers(Request $request){
  $stickers = Sticker::findOrFail($request->s_id);
  foreach($stickers as $sticker){
    $file = $sticker->img;
    $filename = public_path().'/'.$file;
    File::delete($filename);
    $delete = $sticker->delete();
  }

  if($delete){
    Cache::forget('latest_stickers');
    return response()->json("success");
  }else{
    return response()->json("error");
  }
}


public function status($id){
  $sticker = Sticker::findOrFail($id);

  if($sticker->status == 1){
    $sticker->status = 0;
  }else{
    $sticker->status = 1;
  }

  $sticker->save();
  return response()->json($sticker);
}



//Image
public function image_rotate_left(Request $request)
{
  $file = $request->file;

  $image = $file;

  $img = Image::make(public_path($image));
  $img->rotate(90);
  $img->save(public_path($image));


}

public function image_rotate_right(Request $request)
{
  $file = $request->file;

  $image = $file;

  $img = Image::make(public_path($image));
  $img->rotate(-90);
  $img->save(public_path($image));

}



}
