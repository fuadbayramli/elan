<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Cache;
use App\Category;
use App\SubCategory;
use App\Sticker;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Validator;
use App\Http\Controllers\Controller as Controller;

class AdminCategoryController extends Controller
{

  public function user_access($page_slug){
    $return_val = false;
    if(Auth::check()){
      $permissions = array();
      foreach(Auth::user()->role->role_items()->pluck('slug')->toArray() as $key => $rol_item){
        array_push($permissions,$rol_item);
      }
      if (in_array($page_slug,$permissions)){
        $return_val = true;
      }
    }
    if(!$return_val){
      return abort(403);
    }

  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $this->user_access('index');

    if ($request->has('q')) {
      if(!empty($request->get('q'))){
        $categories = Category::where('name', 'LIKE', "%{$request->get('q')}%")
        ->orderBy('created_at', 'desc')
        ->paginate(50);
      }else{
        $categories = Category::orderBy('created_at', 'desc')->paginate(50);
      }
    }else{
      $categories = Category::orderBy('created_at', 'desc')->paginate(50);
    }

    return view('admin.categories.index', compact( 'categories'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $this->user_access('create');

    return view('admin/categories/create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //Validation
    $validator = Validator::make($request->all(), [
      'name' => 'required|min:2|max:255',
    ],
    [
      'name.required' => 'Kateqoriyanın adını daxil edin',
      'name.min' => 'Kateqoriyanın adı minimum 2 simvol ola bilər',
      'name.max' => 'Kateqoriyanın adı maksimum 255 simvol ola bilər',
    ]
  );

  if($validator->fails()){
    $errors = $validator->errors();
    return view('admin.categories.create', compact('errors'));
  }

  $category = Category::create([
    'name' => $request->name,
    'slug' => str_slug($request->name)
  ]);
  if($category){
    Cache::forget('categories');
    Session::flash('success', 'Yeni kateqoriya yaradıldı.');
  }else{
    Session::flash('error', 'Səhv baş verdi.');
  }
  return redirect('admin/categories');
}

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
  //
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
  $this->user_access('update');

  $category = Category::findOrFail($id);
  return view('admin.categories.edit',compact('category'));
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
  $category = Category::findOrFail($id);
  //Validation
  $validator = Validator::make($request->all(), [
    'name' => 'required|min:2|max:255',
  ],
  [
    'name.required' => 'Kateqoriyanın adını daxil edin',
    'name.min' => 'Kateqoriyanın adı minimum 2 simvol ola bilər',
    'name.max' => 'Kateqoriyanın adı maksimum 255 simvol ola bilər',
  ]
);

if($validator->fails()){
  $errors = $validator->errors();
  return view('admin.categories.edit', compact('errors','category'));
}

#Update sticker
$category = Category::where('id', $id)->update([
  'name'=>$request->name
]);

if($category){
  Cache::forget('categories');
  Session::flash('success', 'Kateqoriya uğurla dəyişdirildi.');
}else{
  Session::flash('error', 'Səhv baş verdi.');
}

return redirect('admin/categories');
}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
  $this->user_access('delete');

  $category = Category::findOrFail($id);
  $category->delete();

  return redirect('/admin/categories');
}
}
