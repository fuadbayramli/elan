<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Session;
use Illuminate\Http\Request;
use App\Category;
use App\City;
use App\SubCategory;
use App\Sticker;
use App\StickerImage;
use Auth;
use Validator;
use File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class StickerController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //
  }

  public function stickers(Request $request)
  {
    $_stickers = Sticker::where('status', 1);

    if(request()->get('sort')){
      if(request()->get('sort') == 0){
        $_stickers->orderBy('id','desc');
      }elseif(request()->get('sort') == 1){
        $_stickers->orderBy('price', 'asc');
      }elseif(request()->get('sort') == 2){
        $_stickers->orderBy('price', 'desc');
      }

      $stickers = $_stickers->with('city')->paginate(8);
      $stickers->appends(request()->query());

    }else{
      $_stickers->orderBy('id','desc');
      $stickers = $_stickers->with('city')->paginate(8);
    }

    if($request->ajax()){

      return view('site.stickers.ajax.sticker',compact('stickers'));

    }else{
      return view('site.stickers.stickers',compact('stickers'));
    }

  }



  public function myStickers(Request $request){
    $stickers = Sticker::where('user_id', Auth::user()->id)
    ->where('status', 1)
    ->orWhere('status', 2)
    ->where('user_id', Auth::user()->id);

    if(request()->get('sort')){
      if(request()->get('sort') == 0){
        $stickers->orderBy('id','desc');
      }elseif(request()->get('sort') == 1){
        $stickers->orderBy('price', 'asc');
      }elseif(request()->get('sort') == 2){
        $stickers->orderBy('price', 'desc');
      }

      $stickers = $stickers->paginate(8);
      $stickers->appends(request()->query());

    }else{
      $stickers->orderBy('id','desc');
      $stickers = $stickers->paginate(8);
    }

    if($request->ajax()){
      return view('site.stickers.ajax.sticker',compact('stickers'));
    }else{
      return view('site.profile.my-stickers', compact('stickers'));
    }

  }




  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $categories = Cache::remember('categories', 86400, function () {
      return Category::select('id','name')->get();
    });

    $cities = Cache::remember('cities', 86400, function () {
      return City::select('id','name','slug')->get();
    });

    return view('site.stickers.addSticker', compact('categories','cities'));
  }

  public function subCategory($id){
    $sub_categories = SubCategory::where('category_id', $id)->get();
    return response()->json($sub_categories);
  }



  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //Validation
    $validator = Validator::make($request->all(), [
      'name' => 'required|min:2|max:255',
      'sub_category_id' => 'required',
      'category' => 'required',
      'city_id'=> 'required',
      'description'=> 'required|min:2',
      'author_name'=> 'required',
      'author_email'=> 'required|email',
      'phone'=> 'required|numeric',
      'price'=> 'required|numeric',
    ],
    [
      'name.required' => 'Elanın adını daxil edin',
      'name.min' => 'Elanın adı minimum 2 simvol ola bilər',
      'name.max' => 'Elanın adı maksimum 255 simvol ola bilər',
      'category.required' => 'Kateqoriyanı daxil edin',
      'sub_category_id.required' => 'Alt kateqoriyanı daxil edin',
      'city_id.required' => 'Şəhəri daxil edin',
      'description.required' => 'Məzmun boş ola bilməz',
      'description.min' => 'Məzmun minimum 2 simvol ola bilər',
      'author_name.required' => 'Müəllif boş ola bilməz',
      'author_email.required' => 'Email boş ola bilməz',
      'author_email.unique' => 'Email artıq istifadə olunub',
      'author_email.email' => 'Emaili düzgün daxil edin',
      'phone.required' => 'Mobil nömrəni daxil edin',
      'phone.numeric' => 'Mobil nömrəni düzgün daxil edin',
      'price.required' => 'Qiyməti daxil edin',
      'price.numeric' => 'Qiyməti düzgün daxil edin',
    ]
  );

  if($validator->fails()){
    $errors = $validator->errors();
    return response()->json(['message' => $errors]);
  }


  //Check auth
  if(Auth::check()){
    $user_id = Auth::user()->id;
  }else{
    $user_id = NULL;
  }

  $sticker = Sticker::create([
    'name'=>$request->name,
    'sub_category_id'=>$request->sub_category_id,
    'city_id'=>$request->city_id,
    'description'=>strip_tags($request->description),
    'author_name'=>$request->author_name,
    'author_email'=>$request->author_email,
    'phone'=>$request->phone,
    'price'=>$request->price,
    'new'=>$request->new ? 1 : 0 ?? 0,
    'status'=>2,
    'user_id' => $user_id
  ]);

  if($sticker){
    #Insert image
    for ($i=0; $i < count($request->photos); $i++) {
      StickerImage::create([
        'sticker_id' => $sticker->id,
        'file_name' => $request->photos[$i]
      ]);
    }

    Cache::forget('stickers');
    Cache::forget('cities');
    return response()->json(['message' => "success"]);
  }else{
    return response()->json(['message' => "error"]);
  }

}


/*
* Add Image
*/

public function uploadImage(Request $request){

  $validator = Validator::make($request->all(), [
    'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096'
  ]);

  if ($validator->fails()) {
    return response()->json([
      'message' => 'Error'
    ], 400);
  }

  $photos = $request->file('file');

  if (!is_array($photos)) {
    $photos = [$photos];
  }

  for ($i = 0; $i < count($photos); $i++) {
    $photo = $photos[$i];
    $name = sha1(date('YmdHis') . str_random(30));
    $filename = $name . '.' . $photo->getClientOriginalExtension();
    $directory = 'images';
    $full_filename = "$directory/$filename";
    $path = public_path($full_filename);

    Image::make($photo->getRealPath())->fit(310,277)->save($path);

    return response()->json([
      'file_name'=>$full_filename
    ]);
  }

  return response()->json([
    'message' => ['success']
  ], 200);

}


/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
  $sticker = Sticker::where('id', $id)
  ->where('status', 1)
  ->orWhere('status', 2)
  ->where('id', $id)
  ->firstOrFail();
  return view('site.stickers.sticker-detail', compact('sticker'));
}




/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{

  //Check auth
  if(Auth::check()){
    $user_id = Auth::user()->id;
  }else{
    abort(404);
  }

  $sticker = Sticker::where('id',$id)->where('user_id',$user_id)->firstOrFail();

  $categories = Cache::remember('categories', 86400, function () {
    return Category::select('id','name')->get();
  });

  $cities = Cache::remember('cities', 86400, function () {
    return City::select('id','name','slug')->get();
  });

  return view('site.stickers.editSticker', compact('categories','cities','sticker'));
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
  $sticker = Sticker::findOrFail($id);
  //Validation
  $validator = Validator::make($request->all(), [
    'name' => 'required|min:2|max:255',
    'sub_category_id' => 'required',
    'category' => 'required',
    'city_id'=> 'required',
    'description'=> 'required|min:2',
    'author_name'=> 'required',
    'author_email'=> 'required|email',
    'phone'=> 'required|numeric',
    'price'=> 'required|numeric',
  ],
  [
    'name.required' => 'Elanın adını daxil edin',
    'name.min' => 'Elanın adı minimum 2 simvol ola bilər',
    'name.max' => 'Elanın adı maksimum 255 simvol ola bilər',
    'category.required' => 'Kateqoriyanı daxil edin',
    'sub_category_id.required' => 'Alt kateqoriyanı daxil edin',
    'city_id.required' => 'Şəhəri daxil edin',
    'description.required' => 'Məzmun boş ola bilməz',
    'description.min' => 'Məzmun minimum 2 simvol ola bilər',
    'author_name.required' => 'Müəllif boş ola bilməz',
    'author_email.required' => 'Email boş ola bilməz',
    'author_email.unique' => 'Email artıq istifadə olunub',
    'author_email.email' => 'Emaili düzgün daxil edin',
    'phone.required' => 'Mobil nömrəni daxil edin',
    'phone.numeric' => 'Mobil nömrəni düzgün daxil edin',
    'price.required' => 'Qiyməti daxil edin',
    'price.numeric' => 'Qiyməti düzgün daxil edin',
  ]
);

if($validator->fails()){
  $errors = $validator->errors();
  return response()->json(['message' => $errors]);
}


#Update sticker
$edit = Sticker::where('id', $id)->update([
  'name'=>$request->name,
  'sub_category_id'=>$request->sub_category_id,
  'city_id'=>$request->city_id,
  'description'=>$request->description,
  'author_name'=>$request->author_name,
  'author_email'=>$request->author_email,
  'phone'=>$request->phone,
  'price'=>$request->price,
  'new'=>$request->new ? 1 : 0 ?? 0,
  'status'=>1,
  'user_id' => Auth::user()->id
]);


if($edit){
    #Insert image
    if($request->photos){
      for ($i=0; $i < count($request->photos); $i++) {
        StickerImage::create([
          'sticker_id' => $sticker->id,
          'file_name' => $request->photos[$i]
        ]);
      }
    }
    Cache::forget('stickers');
    Cache::forget('cities');
    return response()->json(['message' => "success"]);
  }else{
    return response()->json(['message' => "error"]);
  }

}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{

  if(Auth::check()){
    $user_id = Auth::user()->id;
  }else{
    abort(404);
  }

  $sticker = Sticker::where('id', $id)->where('user_id', $user_id)->firstOrFail();
  $file = $sticker->img;
  $filename = public_path().'/'.$file;
  File::delete($filename);

  $sticker->delete();
  Cache::forget('stickers');

  return redirect('/');
}

public function deleteImage($image_id){
  $image = StickerImage::findOrFail($image_id);
  $filename = public_path().'/'.$image->file_name;
  File::delete($filename);
  $delete = $image->delete();
  if($delete){
    Cache::forget('stickers');
    return response()->json("success");
  }else{
    return response()->json("error");
  }
}


public function deleteImages(Request $request){
  $images = StickerImage::findOrFail($request->img_id);
  foreach($images as $image){
    $file = $image->file_name;
    $filename = public_path().'/'.$file;
    File::delete($filename);
    $delete = $image->delete();
  }

  if($delete){
    Cache::forget('stickers');
    return response()->json("success");
  }else{
    return response()->json("error");
  }
}








}
