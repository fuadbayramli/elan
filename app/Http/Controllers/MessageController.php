<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageCreateRequest;
use App\Message;
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class MessageController extends Controller
{

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {

  }



  public function contact(){
    return view('site.other-pages.contact');
  }



  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //Validation
    $validator = Validator::make($request->all(), [
      'email' => 'required|email',
      'name' => 'required',
      'phone' => 'required',
      'message' => 'required',
    ],
    [
      'email.required' => 'Emaili daxil edin',
      'email.email' => 'Emaili düzgün daxil edin',
      'name.required' => 'Adınızı daxil edin',
      'phone.required' => 'Mobil nömrəni daxil edin',
      'message.required' => 'Mesaj boş ola bilməz',
    ]
  );

  if($validator->fails()){
    $errors = $validator->errors();
    return response()->json(['message' => $errors]);
  }

  $message = Message::create([
    'email' => $request->email,
    'name' => $request->name,
    'phone' => $request->phone,
    'message' => $request->message
  ]);

  if($message){
    return response()->json(['message' => "success"]);
  }else{
    return response()->json(['message' => "error"]);
  }

}

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
  //
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{

}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{

}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{

}
}
