<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteInformationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'logo' => 'required',
            'main_email' => 'required',
            'main_number' => 'required',
            'address' => 'required',
            'link' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'main_email.required'  => 'The main email field is required',
            'main_number.required'  => 'The main number field is required',
        ];
    }
}
