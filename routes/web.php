<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//======= Admin =========//

#Dashboard
Route::get('admin','Admin\AdminHomeController@index')->name('admin.index');


#Admin Roles
Route::resource('admin/roles', 'Admin\AdminRoleController', ['names'=>[
    'index'=>'admin.roles.index',
    'create'=>'admin.roles.create',
    'store'=>'admin.roles.store',
    'edit'=>'admin.roles.edit'
]]);

#Admin Users
Route::resource('admin/users', 'Admin\AdminUserController', ['names'=>[
    'index'=>'admin.users.index',
    'create'=>'admin.users.create',
    'store'=>'admin.users.store',
    'edit' => 'admin.users.edit'
]]);


#Admin SiteInformation
Route::resource('admin/site-information', 'Admin\AdminSiteInformationController', ['names'=>[
    'index'=>'admin.site-information.index',
    'create'=>'admin.site-information.create',
    'store'=>'admin.site-information.store',
    'edit'=>'admin.site-information.edit'
]]);

#Admin Cities
Route::resource('admin/cities', 'Admin\AdminCityController', ['names'=>[
    'index'=>'admin.cities.index',
    'create'=>'admin.cities.create',
    'store'=>'admin.cities.store',
    'edit'=>'admin.cities.edit'
]]);

#Admin Categories
Route::resource('admin/categories', 'Admin\AdminCategoryController', ['names'=>[
    'index'=>'admin.categories.index',
    'create'=>'admin.categories.create',
    'store'=>'admin.categories.store',
    'edit'=>'admin.categories.edit'
]]);

#Admin Categories
Route::resource('admin/sub-categories', 'Admin\AdminSubCategoryController', ['names'=>[
    'index'=>'admin.sub-categories.index',
    'create'=>'admin.sub-categories.create',
    'store'=>'admin.sub-categories.store',
    'edit'=>'admin.sub-categories.edit'
]]);

#Admin Messages
Route::resource('admin/messages', 'Admin\AdminMessageController', ['names'=>[

    'index'=>'admin.messages.index',
]]);
Route::post('/delete/messages', 'Admin\AdminMessageController@deleteMessage');

#Admin Sticker

Route::get('/admin/stickers', 'Admin\AdminStickerController@index')->name('admin.stickers.index');
Route::delete('/admin/stickers/delete/{id}', 'Admin\AdminStickerController@destroy');
Route::get('/admin/stickers/{id}/edit', 'Admin\AdminStickerController@edit')->name('admin.stickers.edit');
Route::post('/admin/stickers/{id}/update', 'Admin\AdminStickerController@update');
Route::post('/admin/sticker/addImage', 'Admin\AdminStickerController@uploadImage');
Route::get('/admin/sticker/deleteImage/{id}', 'Admin\AdminStickerController@deleteImage');

Route::post('/delete/stickers', 'Admin\AdminStickerController@deleteStickers');

Route::get('/admin/stickers/status/{id}', [
  'uses' => 'Admin\AdminStickerController@status',
  'as' => 'admin.stickers.status'
]);

Route::post('/admin/stickers/image_rotate_left', ['as' => 'productImageRotateLeft', 'uses' => 'Admin\AdminStickerController@image_rotate_left']);
Route::post('/admin/stickers/image_rotate_right', ['as' => 'productImageRotateRight', 'uses' => 'Admin\AdminStickerController@image_rotate_right']);


#User Stickers
Route::get('/user/{id}/stickers', 'Admin\AdminStickerController@stickersWithUserId');
#City Stickers
Route::get('/city/{id}/stickers', 'Admin\AdminStickerController@stickersWithCityId');
#Category Stickers
Route::get('/{base_slug}/{slug}/stickers', 'Admin\AdminStickerController@stickersWithSubCategoryId');


#Admin RoleItems
Route::resource('admin/role-items', 'Admin\AdminRoleItemController', ['names'=>[
    'index'=>'admin.role-items.index',
]]);





//======== Web ========//

#Home
Route::get('/', 'HomeController@index');

Route::post('/search', 'HomeController@searchJson');

#Contact
Route::get('/contact', 'MessageController@contact');

Route::resource('message', 'MessageController', ['names'=>[
    'store'=>'site.other-pages.contact',
]]);
#About
Route::get('/about', 'HomeController@about');
#stickers
Route::get('/stickers', 'StickerController@stickers');
#Wishlist
Route::resource('/wishlist', 'WishlistController', ['except' => ['create', 'edit', 'show', 'update']]);
Route::post('/wishlist/delete/{id}', 'WishlistController@destroy');
#Add Sticker
Route::get('/sticker/add/getSubCategory/{id}', 'StickerController@subCategory');
Route::get('/sticker/add', 'StickerController@create');
Route::post('/sticker/add', 'StickerController@store');
Route::post('/sticker/addImage', 'StickerController@uploadImage');

#Edit sticker
Route::get('/sticker/{id}/edit', 'StickerController@edit');
Route::post('/sticker/{id}/edit', 'StickerController@update');

#Delete sticker
Route::post('/delete/{id}', 'StickerController@destroy');

#Delete image
Route::get('/stickers/deleteImage/{id}', 'StickerController@deleteImage');

#Delete selected images
Route::delete('/delete/images', 'StickerController@deleteImages');

#Sticker
Route::get('/sticker/{id}', 'StickerController@show');
Route::get('/category/{base_slug}/{slug}', 'HomeController@stickersWithSubCategorySlug');
Route::get('/city/{slug}', 'HomeController@stickersWithCitySlug');

#My stickers
Route::get('/my-stickers', 'StickerController@myStickers');

#Favourite Stickers
Route::get('/favourite-stickers', 'StickerController@favourites');


#Auth
Route::get('logout', 'Auth\LoginController@logout');


Auth::routes();
