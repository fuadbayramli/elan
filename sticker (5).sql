-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 10, 2019 at 08:42 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sticker`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Nəqliyyat', 'neqliyyat', '2019-03-11 20:00:00', '2019-03-19 20:00:00'),
(3, 'Xidmətlər', 'xidmetler', '2019-04-03 03:36:46', '2019-04-03 05:24:55'),
(4, 'Şəxsi əşyalar', 'sexsi-esyalar', '2019-06-07 14:05:17', '2019-06-07 14:05:17'),
(5, 'Elektronika', 'elektronika', '2019-06-07 14:07:20', '2019-06-07 14:07:20'),
(6, 'Uşaq aləmi', 'usaq-alemi', '2019-06-07 14:10:24', '2019-06-07 14:10:24');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Bakı', 'baki', '2019-03-03 20:00:00', '2019-04-05 03:17:07'),
(3, 'Sumqayıt', 'sumqayit', '2019-04-02 08:36:31', '2019-04-05 03:17:01'),
(4, 'Ağdaş', 'agdas', '2019-04-05 02:47:09', '2019-04-05 03:16:57'),
(5, 'Ağcabədi', 'agcabedi', '2019-04-05 02:47:13', '2019-04-05 03:16:52'),
(6, 'Mingəçevir', 'mingecevir', '2019-04-05 02:47:17', '2019-04-05 03:16:40'),
(7, 'Xaçmaz', 'xacmaz', '2019-04-05 02:47:20', '2019-04-05 03:16:33'),
(8, 'Zərdab', 'zerdab', '2019-04-05 02:47:23', '2019-04-05 03:16:24'),
(9, 'Tovuz', 'tovuz', '2019-04-05 02:47:27', '2019-04-05 03:16:19'),
(10, 'Şəmkir', 'semkir', '2019-04-05 02:47:30', '2019-04-05 03:16:13'),
(11, 'Qazax', 'qazax', '2019-04-05 02:47:34', '2019-04-05 03:15:59'),
(12, 'Salyan', 'salyan', '2019-04-05 02:47:37', '2019-04-05 03:15:49'),
(13, 'Neftçala', 'neftcala', '2019-04-05 02:47:40', '2019-04-05 03:15:42'),
(14, 'Füzuli', 'fuzuli', '2019-04-05 02:47:44', '2019-04-05 03:17:26'),
(15, 'Daşkəsən', 'daskesen', '2019-04-05 02:47:51', '2019-04-05 03:15:33'),
(16, 'Biləsuvar', 'bilesuvar', '2019-04-05 02:47:57', '2019-04-05 03:15:27'),
(17, 'Qobustan', 'qobustan', '2019-04-05 02:55:10', '2019-04-05 03:15:22'),
(18, 'Şabran', 'sabran', '2019-04-05 02:55:54', '2019-04-05 03:15:17'),
(19, 'Qax', 'qax', '2019-04-05 02:57:52', '2019-04-05 03:15:04'),
(20, 'Saatlı', 'saatli', '2019-04-05 02:58:00', '2019-04-05 03:14:57'),
(21, 'Kürdəmir', 'kurdemir', '2019-04-05 02:58:09', '2019-04-05 03:14:51'),
(22, 'Naftalan', 'naftalan', '2019-04-05 02:58:16', '2019-04-05 03:14:46'),
(23, 'Masallı', 'masalli', '2019-04-05 02:58:22', '2019-04-05 03:14:42'),
(24, 'Nabran', 'nabran', '2019-04-05 02:58:28', '2019-04-05 03:14:35'),
(25, 'Göyçay', 'goycay', '2019-04-05 02:58:34', '2019-04-05 03:14:30'),
(26, 'Oğuz', 'oguz', '2019-04-05 02:58:42', '2019-04-05 03:14:25'),
(27, 'Ucar', 'ucar', '2019-04-05 02:58:51', '2019-04-05 03:14:20'),
(28, 'Culfa', 'culfa', '2019-04-05 02:58:57', '2019-04-05 03:14:16'),
(29, 'Cəlilabad', 'celilabad', '2019-04-05 02:59:04', '2019-04-05 03:14:12'),
(30, 'Gədəbəy', 'gedebey', '2019-04-05 02:59:12', '2019-04-05 03:14:04'),
(31, 'Ağstafa', 'agstafa', '2019-04-05 02:59:19', '2019-04-05 03:13:58'),
(32, 'Beyləqan', 'beyleqan', '2019-04-05 02:59:27', '2019-04-05 03:13:53'),
(33, 'Gəncə', 'gence', '2019-04-05 02:59:35', '2019-04-05 03:13:48'),
(34, 'Ağsu', 'agsu', '2019-04-05 02:59:43', '2019-04-05 03:13:41'),
(35, 'Zaqatala', 'zaqatala', '2019-04-05 03:00:04', '2019-04-05 03:13:38'),
(36, 'Şamaxı', 'samaxi', '2019-04-05 03:00:13', '2019-04-05 03:13:34'),
(37, 'İsmayıllı', 'ismayilli', '2019-04-05 03:00:19', '2019-04-05 03:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_site_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_18_084756_create_role_items_table', 1),
(4, '2018_11_18_084914_create_roles_table', 1),
(5, '2018_11_18_085600_create_role_items_roles_table', 1),
(6, '2018_11_19_062742_create_users_table', 1),
(7, '2019_03_24_145816_create_sticker_table', 1),
(8, '2018_11_18_085049_create_site_informations_table', 2),
(9, '2019_03_25_143544_create_categories_table', 3),
(10, '2019_03_25_143817_create_sub_categories_table', 3),
(11, '2019_03_25_144607_create_sub_categories_table', 4),
(12, '2019_03_26_063259_create_cities_table', 4),
(13, '2019_03_26_073516_create_categories_table', 5),
(14, '2019_03_26_074354_create_sub_categories_table', 6),
(15, '2019_03_26_081016_create_cities_table', 7),
(16, '2019_04_08_103142_create_sticker_images_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '2019-03-24 13:17:27', '2019-03-24 13:17:27'),
(2, 'user', 'user', '2019-03-12 20:00:00', '2019-03-28 20:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_items`
--

DROP TABLE IF EXISTS `role_items`;
CREATE TABLE IF NOT EXISTS `role_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_items`
--

INSERT INTO `role_items` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'site-informations-manage', 'site-informations-manage', '2019-03-24 12:00:49', '2019-03-24 12:00:49'),
(2, 'users-manage', 'users-manage', '2019-03-24 12:00:49', '2019-03-24 12:00:49'),
(3, 'index', 'index', '2019-03-24 12:00:49', '2019-03-24 12:00:49'),
(4, 'create', 'create', '2019-03-24 12:00:49', '2019-03-24 12:00:49'),
(5, 'update', 'update', '2019-03-24 12:00:49', '2019-03-24 12:00:49'),
(6, 'delete', 'delete', '2019-03-24 12:00:49', '2019-03-24 12:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_items_roles`
--

DROP TABLE IF EXISTS `role_items_roles`;
CREATE TABLE IF NOT EXISTS `role_items_roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_item_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_items_roles_role_item_id_index` (`role_item_id`),
  KEY `role_items_roles_role_id_index` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_items_roles`
--

INSERT INTO `role_items_roles` (`id`, `role_item_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 4, 1, NULL, NULL),
(5, 5, 1, NULL, NULL),
(6, 6, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_information`
--

DROP TABLE IF EXISTS `site_information`;
CREATE TABLE IF NOT EXISTS `site_information` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `main_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `main_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `privacy_policy` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_informations_main_email_unique` (`main_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_information`
--

INSERT INTO `site_information` (`id`, `name`, `main_email`, `main_number`, `address`, `link`, `logo`, `privacy_policy`, `created_at`, `updated_at`) VALUES
(1, 'elan', 'info@yoursite.com', '+12 345 67 09', '8500 lorem, New Ispum, Dolor amet sit 12301', 'www.yoursite.com', '1555232556.png', '<p>asdasdsadasd</p>', '2019-04-14 04:54:28', '2019-04-14 05:15:06');

-- --------------------------------------------------------

--
-- Table structure for table `site_users`
--

DROP TABLE IF EXISTS `site_users`;
CREATE TABLE IF NOT EXISTS `site_users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stickers`
--

DROP TABLE IF EXISTS `stickers`;
CREATE TABLE IF NOT EXISTS `stickers` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_category_id` (`sub_category_id`),
  KEY `city_id` (`city_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stickers`
--

INSERT INTO `stickers` (`id`, `user_id`, `sub_category_id`, `city_id`, `name`, `description`, `img`, `author_name`, `author_email`, `new`, `status`, `price`, `phone`, `created_at`, `updated_at`) VALUES
(77, 1, 15, 5, 'pajarni', '<p><a href=\"https://www.doganoyuncak.com/-itfaiyeci-oyuncaklari-itfaiye-garaj-asansorlu-3-katli-otopark-oyun-seti-erkek-cocuk-oyuncak\" target=\"_blank\">doganoyuncak.com</a></p>', 'images/95601422606f15b06484ea1ac56048ecebd3adeb.jpg', 'Fuad', 'f@mail.ru', '1', '1', 50, '3453543', '2019-04-17 03:55:55', '2019-06-08 09:49:02'),
(79, 1, 14, 5, 'personalni komputerler', '<p>CYBERPOWERPC Gamer Ultra GUA883 Desktop Gaming PC (AMD FX-6300 3.5GHz, 8GB</p>', 'images/e9716dc299d8eac5a94c18d46e936539108ad5a6.jpg', 'Fuad', 'f@mail.ru', '1', '1', 999, '3453543', '2019-04-17 03:56:40', '2019-06-11 10:50:38'),
(81, 1, 12, 22, 'HP Pavilion 15-cs0121nb', '<p>HP Pavilion 15-cs0121nb AZERTY - Coolblue - Before 23:59,&nbsp;</p>', 'images/2a2abce17c16e063b5e3bdd68fb5b210a399e8ba.jpg', 'Fuad', 'f@mail.ru', '1', '1', 2622, '3453543', '2019-05-14 05:57:01', '2019-06-08 09:40:59'),
(82, 1, 11, 22, 'klaviatura', '<p><a href=\"https://www.originativeco.com/products/keyboard-co\" target=\"_blank\">Originative</a></p>', 'images/5892b639753791ee913c3c42c5f560d2059fb11d.jpg', 'Fuad', 'f@mail.ru', '1', '1', 265, '345354', '2019-05-14 05:57:28', '2019-06-08 09:38:36'),
(83, 1, 10, 16, 'samsung a50', '<p>Display: 6.4 Inch FHD+ Infinity U Display Processor: Octa-core Exynos 9610 Octa Processor with 4GB RAM ROM: 64GB ROM | SD Slot Up To 512GB Camera: Triple 25MP+8MP+5MP Rear |&nbsp;</p>', 'images/a87d0643ab6f97a8098c732cd0f86b2f23780e09.jpg', 'Fuad', 'f@mail.ru', '1', '1', 1235, '6546', '2019-05-14 06:03:22', '2019-06-08 09:35:43'),
(84, 1, 9, 21, 'Brooks Women\'s Ghost', '<p>This Brooks Women\'s Ghost 11 Navy-Grey-Blue has been updated for an improved fit and increased breathability, perfect for your next run</p>', 'images/700e5327f80860fe51896a71b223fbd66af2d006.jpg', 'Fuad', 'f@mail.ru', '1', '1', 123, '2342342342343', '2019-05-14 06:09:35', '2019-06-11 11:02:51'),
(85, 1, 8, 21, 'The Bamboo Accessories', '<p>Bamboo is the material of the season. We&#39;ve rounded up our favorite finds! From bags &amp; jewelry to home decor, here&#39;s how to rock the trend.</p>', 'images/ded77029b9eaa41c069392937ed46511c02f9498.png', 'Fuad', 'f@mail.ru', '1', '1', 134, '41', '2019-05-14 06:13:25', '2019-06-08 09:30:55'),
(86, 1, 7, 19, 'Geonardo Bravo Black Dial Chain', '<p>Geonardo Retail - offering Geonardo Bravo Black Dial Chain Men\'s Watch at Rs 200/piece in Delhi, Delhi. Get best price and read about company and get contact details and address.</p>', 'images/9207d3d75f4da37b5dae617eae1b87a8f670977a.jpg', 'Fuad', 'f@mail.ru', '1', '1', 6787, '45354354', '2019-05-14 06:18:33', '2019-06-10 06:27:03'),
(87, 1, 6, 19, 'Top 5 Technology Trends', '<p><a href=\"https://newelectronicsguide.com/top-5-technology-trends-which-will-drive-the-future/\" target=\"_blank\">newelectronicsguide.com</a></p>', 'images/faa3decb669c272bcd1f1de4e6cbe0978fd125d2.jpg', 'Fuad', 'f@mail.ru', '1', '1', 1566, '4535354354', '2019-05-14 06:20:14', '2019-06-11 12:52:26'),
(88, 1, 1, 21, '2019 Honda Accord', '<p>The 2019 Honda Accord sets the bar for midsize sedans with a technology-packed interior and a restyled sporty exterior.</p>', 'images/a80ec04219ba42ae11231829205ae088a2b3fd67.png', 'Fuad', 'f@mail.ru', '1', '1', 15616, '00000000', '2019-05-14 06:21:16', '2019-06-12 12:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `sticker_images`
--

DROP TABLE IF EXISTS `sticker_images`;
CREATE TABLE IF NOT EXISTS `sticker_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sticker_id` int(10) UNSIGNED NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sticker_images_sticker_id_foreign` (`sticker_id`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sticker_images`
--

INSERT INTO `sticker_images` (`id`, `sticker_id`, `file_name`, `created_at`, `updated_at`) VALUES
(55, 71, 'images/70578f1da598616252365362ccd11971945b0cbf.png', '2019-04-09 04:32:20', '2019-04-09 04:32:20'),
(54, 71, 'images/d367216734b715b65ac788cefb210657e89df0e7.jpg', '2019-04-09 04:32:20', '2019-04-09 04:32:20'),
(53, 69, 'images/15547298022.PNG', '2019-04-08 09:23:22', '2019-04-08 09:23:22'),
(52, 69, 'images/15547298021.PNG', '2019-04-08 09:23:22', '2019-04-08 09:23:22'),
(51, 69, 'images/15547298020.PNG', '2019-04-08 09:23:22', '2019-04-08 09:23:22'),
(62, 73, 'images/9b3610f1845283837aa4135a48bd167e006e049e.PNG', '2019-04-13 12:37:46', '2019-04-13 12:37:46'),
(63, 74, 'images/6e86d70674342b84c5b1ed2979153386b3a40369.png', '2019-04-16 05:22:07', '2019-04-16 05:22:07'),
(64, 75, 'images/4e8a7abbbe993dc12ecead5863c7e66f47ecb462.png', '2019-04-16 05:22:31', '2019-04-16 05:22:31'),
(140, 76, 'images/975593700284d1c43bc2c4dc4bfc674d83e286fa.jpg', '2019-06-08 09:51:18', '2019-06-08 09:51:18'),
(137, 77, 'images/95601422606f15b06484ea1ac56048ecebd3adeb.jpg', '2019-06-08 09:48:55', '2019-06-08 09:48:55'),
(135, 79, 'images/e9716dc299d8eac5a94c18d46e936539108ad5a6.jpg', '2019-06-08 09:46:35', '2019-06-08 09:46:35'),
(134, 79, 'images/2fb19d36d5875b751f97e428c4cf55479fc053d7.jpg', '2019-06-08 09:46:35', '2019-06-08 09:46:35'),
(133, 79, 'images/a34c5c977fce5cafb91792710c579cdedcc4bf1f.jpg', '2019-06-08 09:46:35', '2019-06-08 09:46:35'),
(136, 77, 'images/b57d14f7d954faab86340f0cf703a34c2977d385.jpg', '2019-06-08 09:48:55', '2019-06-08 09:48:55'),
(132, 80, 'images/ab5c9781fe714e6c93744bc8bbe325b4f8780507.jpg', '2019-06-08 09:43:32', '2019-06-08 09:43:32'),
(131, 80, 'images/7bee2d139fd0a928be864fba39f0e88818400cf1.jpg', '2019-06-08 09:43:32', '2019-06-08 09:43:32'),
(130, 80, 'images/03799547bba7afb4b36645440cfe92381462d72d.jpg', '2019-06-08 09:43:32', '2019-06-08 09:43:32'),
(129, 80, 'images/005487b0964d0918f4fee0865bb7f1e1e46c12c5.jpg', '2019-06-08 09:43:32', '2019-06-08 09:43:32'),
(128, 81, 'images/2a2abce17c16e063b5e3bdd68fb5b210a399e8ba.jpg', '2019-06-08 09:40:50', '2019-06-08 09:40:50'),
(127, 81, 'images/53f9ffe122bc82176e1324b6f4d53236010d13b7.jpg', '2019-06-08 09:40:50', '2019-06-08 09:40:50'),
(125, 81, 'images/36be37b1ffc4e937093f5e8e606ec1b2d87e1755.png', '2019-06-08 09:40:50', '2019-06-08 09:40:50'),
(126, 81, 'images/cafc0369eb74e571d1b46df330f271be57a3ac20.jpg', '2019-06-08 09:40:50', '2019-06-08 09:40:50'),
(124, 82, 'images/5892b639753791ee913c3c42c5f560d2059fb11d.jpg', '2019-06-08 09:38:28', '2019-06-08 09:38:28'),
(123, 82, 'images/8681714568a86f33f66eebd9e7e15e20c08b8948.jpg', '2019-06-08 09:38:28', '2019-06-08 09:38:28'),
(122, 82, 'images/68392a329f0520071bb9d57a0fef9bf8424b778d.jpg', '2019-06-08 09:38:28', '2019-06-08 09:38:28'),
(104, 89, 'images/65a66d5bae51a2d919561575db794aff13a92127.jpg', '2019-06-08 09:16:18', '2019-06-08 09:16:18'),
(120, 83, 'images/a87d0643ab6f97a8098c732cd0f86b2f23780e09.jpg', '2019-06-08 09:35:33', '2019-06-08 09:35:33'),
(119, 83, 'images/4b9dd2b55fb3e028cbce6b870592f31859f826a2.jpg', '2019-06-08 09:35:33', '2019-06-08 09:35:33'),
(117, 84, 'images/61d8ea17b01f94def3f4050009b0edea7ed331a9.jpg', '2019-06-08 09:33:16', '2019-06-08 09:33:16'),
(116, 84, 'images/700e5327f80860fe51896a71b223fbd66af2d006.jpg', '2019-06-08 09:33:16', '2019-06-08 09:33:16'),
(115, 84, 'images/54b039b61d772f2f2f933b57dff8aa33950dd45f.jpg', '2019-06-08 09:33:16', '2019-06-08 09:33:16'),
(113, 85, 'images/ded77029b9eaa41c069392937ed46511c02f9498.png', '2019-06-08 09:30:43', '2019-06-08 09:30:43'),
(112, 85, 'images/9bbc1666a8cbf193f8d91b877a21a3786f27a720.jpg', '2019-06-08 09:30:43', '2019-06-08 09:30:43'),
(111, 86, 'images/9207d3d75f4da37b5dae617eae1b87a8f670977a.jpg', '2019-06-08 09:27:43', '2019-06-08 09:27:43'),
(110, 86, 'images/24ec75633c7f2cb4a410e34547dea4eefdf33096.jpg', '2019-06-08 09:27:43', '2019-06-08 09:27:43'),
(109, 86, 'images/67fc91d84223da9ac2908b608d85747466194717.jpg', '2019-06-08 09:27:43', '2019-06-08 09:27:43'),
(114, 85, 'images/7cda5eb619a5d685d87b7320a1782df9e0bdaa54.jpg', '2019-06-08 09:30:43', '2019-06-08 09:30:43'),
(107, 87, 'images/faa3decb669c272bcd1f1de4e6cbe0978fd125d2.jpg', '2019-06-08 09:24:08', '2019-06-08 09:24:08'),
(108, 87, 'images/be9fd99d67ee884fde33e1254878f405d2a7eb48.jpeg', '2019-06-08 09:24:08', '2019-06-08 09:24:08'),
(106, 88, 'images/a80ec04219ba42ae11231829205ae088a2b3fd67.png', '2019-06-08 09:19:56', '2019-06-08 09:19:56'),
(105, 88, 'images/387b43edf6cfe850130edf071ec7a3a5c84cac6a.jpg', '2019-06-08 09:19:56', '2019-06-08 09:19:56'),
(102, 89, 'images/e972b3611da05e85ad42fc581f5dc7c8c7f672ac.jpg', '2019-06-08 09:16:18', '2019-06-08 09:16:18'),
(103, 89, 'images/eea8048d22c1b462264982478580dd1470261db8.jpg', '2019-06-08 09:16:18', '2019-06-08 09:16:18'),
(118, 84, 'images/9f075363086a5b288a170146c373777de0e2c4e5.jpg', '2019-06-08 09:33:16', '2019-06-08 09:33:16'),
(121, 83, 'images/0b9deded12c0f3e4967c71b866e617ba0926ed75.jpg', '2019-06-08 09:35:33', '2019-06-08 09:35:33'),
(138, 77, 'images/53afcaafa499129978fa972d7ad4f230e29e38c8.jpg', '2019-06-08 09:48:55', '2019-06-08 09:48:55'),
(139, 77, 'images/33fc080d5357a17eb27cc34760ebf9caf66c5cd7.jpg', '2019-06-08 09:48:55', '2019-06-08 09:48:55'),
(141, 76, 'images/d6af42e7a1fec4fe2b7bc7d2a262dcfed9bb377d.jpg', '2019-06-08 09:51:18', '2019-06-08 09:51:18'),
(142, 76, 'images/95c793efeadbb37c939b2d7e1693b32d38426b00.jpg', '2019-06-08 09:51:18', '2019-06-08 09:51:18'),
(143, 93, 'images/c9d2b48c2218f506a0ad682d2b26217c0af557ef.jpg', '2019-06-13 12:07:19', '2019-06-13 12:07:19');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 1, 'Avtomobillər', 'avtomobiller', '2019-03-11 20:00:00', '2019-03-12 20:00:00'),
(2, 1, 'Avtobuslar və xüsusi texnikalar', 'avtobuslar-ve-xususi-texnikalar', '2019-03-21 20:00:00', '2019-03-21 20:00:00'),
(5, 3, 'Hüquq xidmətləri', 'huquq-xidmetleri', '2019-04-03 04:17:25', '2019-04-03 04:17:25'),
(6, 3, 'İt,internet,telekom', 'itinternettelekom', '2019-04-03 04:17:59', '2019-04-03 05:27:07'),
(7, 4, 'Saat və zinət əşyaları', 'saat-ve-zinet-esyalari', '2019-06-07 14:05:54', '2019-06-07 14:05:54'),
(8, 4, 'Aksesuarlar', 'aksesuarlar', '2019-06-07 14:06:11', '2019-06-07 14:06:11'),
(9, 4, 'Geyim və ayaqqabılar', 'geyim-ve-ayaqqabilar', '2019-06-07 14:06:50', '2019-06-07 14:06:50'),
(10, 5, 'Telefonlar', 'telefonlar', '2019-06-07 14:07:36', '2019-06-07 14:07:36'),
(11, 5, 'Kompüter aksesuarları', 'komputer-aksesuarlari', '2019-06-07 14:07:49', '2019-06-07 14:07:49'),
(12, 5, 'Noutbuklar və Netbuklar', 'noutbuklar-ve-netbuklar', '2019-06-07 14:08:23', '2019-06-07 14:08:23'),
(13, 5, 'Oyunlar, pultlar və proqramlar', 'oyunlar-pultlar-ve-proqramlar', '2019-06-07 14:08:59', '2019-06-07 14:08:59'),
(14, 5, 'Masaüstü kompüterlər', 'masaustu-komputerler', '2019-06-07 14:09:12', '2019-06-07 14:09:12'),
(15, 6, 'Oyuncaqlar', 'oyuncaqlar', '2019-06-07 14:11:39', '2019-06-07 14:11:39'),
(16, 6, 'Uşaq arabaları və avtomobillər', 'usaq-arabalari-ve-avtomobiller', '2019-06-07 14:11:50', '2019-06-07 14:11:50'),
(17, 6, 'Uşaq mebeli', 'usaq-mebeli', '2019-06-07 14:12:01', '2019-06-07 14:12:01'),
(18, 6, 'Uşaq qidası', 'usaq-qidasi', '2019-06-07 14:12:13', '2019-06-07 14:12:13'),
(19, 6, 'Uşaq geyimi', 'usaq-geyimi', '2019-06-07 14:12:25', '2019-06-07 14:12:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `img`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fuad', '1555180218.jpg', 'f@mail.ru', NULL, '$2y$10$wazhSeKKlRK9qlPpUxPipO7tuqk2KCSysWR0AAZLzvM0h.8Vx5DDG', 'BQgIEFv3l3vQKI5aWEbsYnjM3XdzNDtyn9Tn5Fj5QNEpCB2L39tCbxq5FEqJ', '2019-03-24 13:20:27', '2019-04-13 14:30:18'),
(2, 2, 'Zeynal', '1555180453.JPG', 'zeynaloffnet@gmail.com', NULL, '$2y$10$eA0PoL4XtbDjeRZGtxTTSeTW3Nfe3RnauzCcMeZa0FOBS05K6bQoO', NULL, '2019-03-25 03:32:53', '2019-04-13 14:34:13'),
(10, 2, 'Ballarimiz', '1555180464.jpg', 'fuadbayramli94@gmail.com', NULL, '$2y$10$DURzwqO7WDk0FSREfEFoL.UuSvCjQbwYh5PiE7UPf702BqvtL2n7q', NULL, '2019-03-25 05:25:12', '2019-04-13 14:34:24'),
(11, 2, 'Elvin', '1555180470.jpg', 'fuad@mail.ru', NULL, '$2y$10$YZtfoc/kN67qQpUWJIY3BuHgzZlrKULRsD.sAJbaW5pUhyDXk1arC', NULL, '2019-03-25 07:08:57', '2019-04-17 07:08:58'),
(12, NULL, 'sdaa sdasda', '1559894631.PNG', 'f@mail.ruuadw', NULL, '$2y$10$eT0QLlEHrIomAE8bRShWdO6qLqGS7BUWFszKyoUsMypzVQBkkcXRW', NULL, '2019-05-27 12:43:44', '2019-06-07 08:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

DROP TABLE IF EXISTS `wishlists`;
CREATE TABLE IF NOT EXISTS `wishlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sticker_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=309 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `user_id`, `sticker_id`, `created_at`, `updated_at`) VALUES
(308, 1, 89, '2019-06-27 12:03:28', '2019-06-27 12:03:28'),
(294, 1, 82, '2019-06-10 10:54:00', '2019-06-10 10:54:00'),
(301, 1, 81, '2019-06-11 16:27:46', '2019-06-11 16:27:46'),
(275, 1, 79, '2019-05-21 13:42:51', '2019-05-21 13:42:51'),
(289, 1, 77, '2019-05-21 13:59:21', '2019-05-21 13:59:21');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_items_roles`
--
ALTER TABLE `role_items_roles`
  ADD CONSTRAINT `role_items_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_items_roles_role_item_id_foreign` FOREIGN KEY (`role_item_id`) REFERENCES `role_items` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `stickers`
--
ALTER TABLE `stickers`
  ADD CONSTRAINT `stickers_ibfk_1` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stickers_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stickers_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
