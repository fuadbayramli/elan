<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @yield('css')
  <title>Classify</title>
  <!-- Fav Icon -->
  <link rel="shortcut icon" href="favicon.ico">
  <!-- Scripts -->

  <!-- Fonts -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <link href="{{ asset('frontend/classified/css/owl.carousel.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/classified/css/font-awesome.css') }}" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300italic,300,400italic,500italic,500,700,700italic' rel='stylesheet' type='text/css'>
  <link href="{{ asset('frontend/classified/css/style.css') }}" rel="stylesheet">

</head>
<body>
  <!--top bar start-->
  <div class="topbar-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-6 col-xs-6" id="social">
          <ul class="social-wrap">
            <li><a href="#."><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
            <li><a href="#."><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
            <li><a href="#."><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
            <li><a href="#."><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
            <li><a href="#."><i class="fa fa-vimeo-square" aria-hidden="true"></i></a></li>
          </ul>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-6">
          <div class="user-wrap">
            @guest
              <div class="login-btn">
                <a href="{{ route('login') }}">{{ __('Login') }}</a>
              </div>
              @if (Route::has('register'))
                <div class="register-btn">
                  <a href="{{ url('register') }}">{{ __('Register') }}</a>
                </div>
                <div class="clearfix"></div>
              @endif
            @else
              <div class="dropdown">
                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  {{ Auth::user()->name }}
                  <span class="caret"></span>
                </button>
                <div id="my_stickers">
                    <a href="{{ url('/my-stickers') }}" class="btn btn-primary">Mənim elanlarım</a>
                </div>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <li>
                    @if(Auth::user()->role_id == 1)
                      <a class="dropdown-item" href="{{url('admin')}}">
                        Admin panel
                      </a>
                    @endif
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                  </li>
                </ul>
                <div class="favourite">
                  <a href="{{url('/wishlist')}}" class="btn btn-warning">Seçilmişlər</a>
                </div>
              </div>
          @endguest

        </div>
      </div>


    </div>
  </div>
</div>
<!--top bar start end-->

@php
  $site_informations = \Cache::remember('site_informations', 86400, function () {
    return App\SiteInformation::get();
  });
@endphp

<!--header start-->
<div class="header-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-12">
        <div class="logo"><a href="{{url('/')}}"><img src="{{ asset('images')."/".$site_informations->first()->logo }}" alt="logo"></a></div>
      </div>
      <div class="col-md-7 col-sm-9">
        <div class="navigationwrape">
          <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="dropdown"> <a href="{{url('/')}}" class="{{ (Request::url() == url('/')) ? 'active' : '' }}"> Ana səhifə</a></li>
                <li> <a href="{{ url('/about') }}" class="{{ (Request::url() == url('/about')) ? 'active' : '' }}"> Haqqımızda </a></li>
                <li> <a href="{{ url('/stickers') }}" class="{{ (Request::url() == url('/stickers')) ? 'active' : '' }}"> Elanlar</a></li>
                <li> <a href="{{ url('/contact') }}" class="{{ (Request::url() == url('/contact')) ? 'active' : '' }}"> Bizimlə əlaqə </a></li>
              </ul>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-md-2 col-sm-3">
        <div class="post-btn"><a href="{{ url('/sticker/add') }}">Elan yerləşdir</a></div>
      </div>
    </div>
  </div>
</div>
<!--header start end-->

<main class="py-4">
  @yield('content')
</main>

<!--footer start-->
<div class="footer-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6">
        <h3>Haqqımızda</h3>
        <div class="footer-logo"><a href="{{url('/')}}"><img src="{{ asset('images')."/".$site_informations->first()->logo }}" alt="img"></a></div>
        <p>TAP.AZ - Azərbaycanın ən məşhur və stabil artan internet resurslarından biridir. Hər gün bu saytı minlərlə insan ziyarət edir... <a href="{{url('/about')}}">Ətraflı</a></p>
      </div>
      <div class="col-md-2 col-sm-6">
        <h3>Sürətli keçidlər</h3>
        <ul class="footer-links">
          <li><a href="{{ url('/') }}">Ana səhifə</a></li>
          <li><a href="{{ url('/about')}}">Haqqımızda</a></li>
          <li><a href="{{ url('/stickers') }}">Elanlar</a></li>
          <li><a href="{{ url('/contact') }}">Bizimlə əlaqə</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-sm-6">
      </div>
      <div class="col-md-3 col-sm-6">
        <h3>Bizimlə əlaqə</h3>
        <div class="address">{{$site_informations->first()->address }}</div>
        <div class="info"><i class="fa fa-phone" aria-hidden="true"></i> <a>{{$site_informations->first()->main_number }}</a></div>
        <div class="info"><i class="fa fa-fax" aria-hidden="true"></i> <a>{{$site_informations->first()->main_number }}</a></div>
      </div>
    </div>
    <div class="copyright">Copyright © 2017 Classify - Bütün hüquqlar qorunur.</div>
  </div>
</div>

<!--footer end-->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<!-- general script file -->
<script src="{{ asset('frontend/classified/js/owl.carousel.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('frontend/classified/js/script.js') }}"></script>


@yield('js')
</body>
</html>
