@extends('site.layouts.app')
  @section('css')
    <link rel="stylesheet" type="text/css"  href="{{ asset('css/dropzone.css') }}">
  @endsection
@section('content')
  <!--inner heading start-->
  <div class="inner-heading">
    <div class="container">
      <h1>Yeni elan</h1>
    </div>
  </div>
  <!--inner heading end-->

  <!--ad post start-->
  <div class="inner-wrap listing">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-2"></div>
        <div class="col-md-6 col-sm-8">
          <div class="login">
            <div class="contctxt">İnformasiyaları əlavə edin</div>
            <div id="errorResponse">

            </div>
            <div class="formint conForm">
              <form class="form-group" action="" id="addSticker" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <select class="form-control" name="category" id="category">
                        <option value="">Kateqoriyalar</option>
                        <?php foreach ($categories as $key => $category): ?>
                          <option value="{{ $category->id }}">{{ $category->name }}</option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <select class="form-control" name="sub_category_id" id="sub_category">
                        <option value="">Alt Kateqoriyalar</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="input-wrap">
                  <input type="text" name="name" class="form-control" placeholder="Elanın adı">
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <input type="text" name="price" placeholder="Qiymət" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap row">
                      <label for="checkbox" class="col-md-6"><h3>Yeni</h3></label>
                      <input type="checkbox" name="new" id="checkbox" class="form-control col-md-6">
                    </div>
                  </div>
                </div>
                <div class="input-wrap">
                  <textarea name="description" rows="5" cols="80" class="form-control" placeholder="Məzmun"></textarea>
                </div>
                <div class="input-wrap">
                  <div id="myDropZone" class="dropzone dropzone-design">
                    <div class="dz-default dz-message"><span>Şəkilləri bura yerləşdirin</span></div>
                  </div>
                  <div id="productImages"></div>
                </div>

                <div class="contctxt persional">Şəxsi İnformasiyalar</div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <input type="text" name="author_name" class="form-control"
                      value="{{ (Auth::check()) ? Auth::user()->name : '' }}" placeholder="Adınız">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <input type="text" name="author_email" class="form-control"
                      value="{{ (Auth::check()) ? Auth::user()->email : '' }}" placeholder="Emailiniz">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <input type="text" name="phone" placeholder="Mobil nömrə" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <select class="form-control" name="city_id">
                        <option value="">Şəhər seçin</option>
                        <?php foreach ($cities as $key => $city): ?>
                          <option value="{{ $city->id }}">{{ $city->name }}</option>
                        <?php endforeach; ?>
                      </select>
                  </div>
                </div>
                <div class="sub-btn">
                  <button type="submit" id="addSticker_submit" class="sbutn">Əlavə et</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-2"></div>
      </div>
    </div>
  </div>
  <!--ad post end-->


@endsection

@section('js')
  <script type="text/javascript" src="{{ asset('/js/dropzone.js') }}"></script>
  <script>

  Dropzone.autoDiscover = false;

  $(function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("div#myDropZone").dropzone({
      url: "{{ url('/sticker/addImage') }}",
      sending: function(file, xhr, formData) {
        formData.append("_token", CSRF_TOKEN);
      },
      success:function(re){
        var obj = $.parseJSON(re.xhr.response);
        $("#productImages").append('<input type="hidden" value="'+ obj.file_name +'" name="photos[]"/>');
      }
    });
  });

  $("#category").change(function(){
    $("#sub_category").empty();
    $.ajax({
      type: 'GET',
      url: '{{ url('/sticker/add/getSubCategory') }}/'+$(this).val(),
      dataType: 'json',
      success: function (data) {
        $.each(data, function(index, element) {
          $('#sub_category').append('"<option value="'+element.id+'">' + element.name +'</option>');
        });
      }
    });
  });


  $("#addSticker").submit(function(){
    $("#addSticker_submit").text("Əlavə edilir...").css('opacity','0.6');
    $.ajax({
      url:"{{url('/sticker/add')}}",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      method:"POST",
      data:$(this).serialize(),
      success: function(response){
        $('html, body').animate({scrollTop:$('#errorResponse').offset().top}, 'slow');
        if(response.message == "success"){
          if ($(".alert")[0]){
            $("#errorResponse").removeClass();
          }
          $("#errorResponse").empty().html('<div class="alert alert-success">Əlavə edildi</div>');
          $(".form-control").val("");
          $("#checkbox").prop("checked", false);
          Dropzone.forElement('#myDropZone').removeAllFiles(true)
        }else{
          $("#errorResponse").empty();
          $("#errorResponse").addClass('alert alert-danger');
          $.each(response.message, function( k, v ) {
              $("#errorResponse").append('<span>'+v+'</span><br>');
            });
        }
        $("#addSticker_submit").text("Əlavə et").css('opacity','1');
      },
      error: function(response){
        $("#errorResponse").empty().html('<div class="alert alert-danger">Səhv baş verdi</div>');
        $("#addSticker_submit").text("Əlavə et").css('opacity','1');
      }
    });

    return false;
  });
</script>
@endsection
