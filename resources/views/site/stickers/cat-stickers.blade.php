@extends('site.layouts.app')

@section('content')
  <!--inner heading start-->
  <div class="inner-heading">
    <div class="container">
      <h1>{{$title}}</h1>
    </div>
  </div>
  <!--inner heading end-->
  <div class="slider-wrap">
    <div class="container">
      <div class="sliderTxt">
        <form action="{{ url('/') }}" method="get">
          <div class="row form-wrap">
            <div class="col-md-3" id="search">
              <div class="input-group">
                <?php $q = request()->get('q') ?? '' ?>
                <input type="text" id="search-box" class="form-control" name="q" value="{{$q}}" autocomplete="off" placeholder="Axtarın...">
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-group">
                <select class="dropdown" id="category">
                  <option value="">Kateqoriyalar</option>
                  <?php foreach ($categories as $key => $category): ?>
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-group">
                <select class="dropdown" name="sub_category" id="sub_category">
                  <option value="">Alt Kateqoriyalar</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-btn">
                <button type="submit" class="sbutn">Axtar</button>
              </div>
            </div>
            <div class="col-md-12" id="suggestion-box">
              <ul id="sticker-list">
                  <li onClick="selectSticker({{$q}});"></li>
              </ul>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <br>
    @if (Session::has('message'))
      <div class="alert alert-danger container">{{ Session::get('message') }}</div>
    @endif

    <!--feature start-->
    <div class="feature-wrap">
      <div class="container">
        <div class="heading-title">Elanlar</div>
        <div class="row">
          <div class="col-md-2" id="sort">
            <select id="sort_select" class="form-control" name="sort">
              <option value="">Sırala</option>
              <option value="0">Son elanlar</option>
              <option value="1">Qiymət (azdan çoxa)</option>
              <option value="2">Qiymət (çoxdan aza)</option>
            </select>
          </div>
        </div>
          @if(count($stickers)!=0)
            <ul class="row feature-service" id="row">
              @foreach($stickers as $sticker)
                @include('site.stickers.includes.sticker')
              @endforeach
            </ul>
            <div class="view-btn"><button type="submit" id="more"><a>Daha çox</a></button></div>
          @else
            <h3 class="alert alert-danger">
              Elan tapılmadı
            </h3>
          @endif
      </div>
    </div>
    <!--feature end-->


    <!--Categories start-->
    <div class="categories-wrap">
      <div class="container">
        <div class="heading-title"><span>Kateqoriyalar</span></div>
        <ul class="row categories-service">
          @foreach($categories as $category)
            <li class="col-md-3 col-sm-6">
              <div class="categorybox">
                <p class="color">
                    <div class="icon"><i class="fa fa-briefcase" aria-hidden="true"></i></div>
                </p>
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse_{{$category->id}}" aria-expanded="false" aria-controls="collapseExample">
                  <h4>{{$category->name}}</h4>
                </button>
                <div class="collapse" id="collapse_{{$category->id}}">
                  <div class="card card-body">
                    <ul class="categories">
                      @foreach($category->sub_categories as $sub_cat)
                        <li><a href="{{ url('/category').'/'.$sub_cat->category->slug.'/'.$sub_cat->slug }}">{{$sub_cat->name}}({{count($sub_cat->stickers)}})</a></li>
                      @endforeach
                    </ul>
                  </div>
                </div>

              </div>

            </li>
          @endforeach
        </ul>
      </div>
    </div>
    <!--Categories end-->

@endsection

@section('js')
  <script>

  var page_number =2

  $("#more").click(function(){
    $("#more a").text('Yüklənir...');
    $.ajax({
      type: 'GET',
      url: '/elan/@isset($sub_category)category/{{$sub_category->category->slug}}/{{$sub_category->slug}}@elseif(isset($city))city/{{$city->slug}}@endisset'+'/?page='+page_number+'&sort='+$("#sort_select").val(),
      success: function (response) {
        $("#row").append(response);
        page_number+=1;
        $("#more a").text('Daha çox');
        if(response==""){
          $("#more a").hide();
        }

      }
    });

  });

  $("#sort_select").change(function(){
    page_number=1;
    $("#row").empty();
    $("#more a").show();
    $("#more a").text('Yüklənir...');
    $.ajax({
      type: 'GET',
      url: '/elan/@isset($sub_category)category/{{$sub_category->category->slug}}/{{$sub_category->slug}}@elseif(isset($city))city/{{$city->slug}}@endisset'+'/?page='+page_number+'&sort='+$("#sort_select").val(),
      success: function (response) {
        $("#row").append(response);
        page_number+=1;
        $("#more a").text('Daha çox');
        if(response==""){
          $("#more a").hide();
        }

      }
    });

  });



  $("#category").change(function(){
    $("#sub_category").empty();
    $.ajax({
      type: 'GET',
      url: '{{ url('/sticker/add/getSubCategory') }}/'+$(this).val(),
      dataType: 'json',
      success: function (data) {
        $.each(data, function(index, element) {
          $('#sub_category').append('"<option value="'+element.id+'">' + element.name +'</option>');
        });
      }
    });
  });




  $("#search-box").keyup(function(){
    if($("#search-box").val().length>3){
      $('#sticker-list').empty();
      $.ajax({
        type: "POST",
        url: '{{url('/search')}}',
        data: { q:$(this).val(),
          "_token": "{{ csrf_token() }}" },
        beforeSend: function(){
          $("#search-box").css("background","#FFF url({{asset('images/image-loader.gif')}}) no-repeat 12px");
        },
        success: function(data){
          $("#suggestion-box").show();
          $.each(data, function(index, element) {
            $('#sticker-list').css("background","#fff").append('<li><a href="{{url('/?q=')}}'+$("#search-box").val()+'">' + element.name +'</a></li>');
          });
          $("#search-box").css("background","#fff");
        }
      });
    }else{
      $("#sticker-list").empty();
    }
  });



  function selectSticker(val) {
  $("#search-box").val(val);
  $("#suggestion-box").hide();
  }
</script>

@endsection
