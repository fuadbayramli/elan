<li class="col-md-3 col-sm-6 col-xs-12" id="f_{{ $sticker->id }}">
  <div class="feature-image"><img src="{{ asset($sticker->img)}}" alt="img">
    <div class="price">{{ $sticker->price}} Azn</div>
  </div>
  <div class="feature">
    <div class="feat-bg">
      <h3><a href="{{url('sticker').'/'.$sticker->id}}">{{ str_limit($sticker->name, 20) }}</a></h3>
    </div>
    <div class="feature-detail">
      <ul class="featureList">
        <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{$sticker->city->name}}</li>
        <li><i class="fa fa-clock-o" aria-hidden="true"></i> {{$sticker->updated_at->format('d M Y')}}</li>
      </ul>
    </div>
  </div>
</li>
