@extends('site.layouts.app')
@section('css')
  <link href="{{ asset('backend/css/sweet-alert.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('frontend/classified/css/flexslider.css')}}" type="text/css" media="screen" />
@endsection
@section('content')

  <!--inner heading start-->
  <div class="inner-heading">
    <div class="container">
      <h1>{{$sticker->name}}</h1>
    </div>
  </div>
  <!--inner heading end-->

  <!--Detail page start-->
  <div class="inner-wrap about">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div id="main" role="main">
            <div class="slider">
              <div class="flexslider innerslider">
                <ul class="slides">
                  @foreach ($sticker->images as $image)
                    <li data-thumb="{{ asset($image->file_name) }}"> <img src="{{ asset($image->file_name) }}" alt="img"/> </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
          <h2>Təsvir</h2>
          <p>{!!$sticker->description!!}</p>

        </div>
        <div class="col-md-4">
          @if ($sticker->status == 1)
            <div class="sidebarWrp">
              <div class="userinfo">
                <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                <h3>{{$sticker->author_name}}</h3>
              </div>
              <div class="innerprice">{{$sticker->price}} Azn</div>
              <div class="phone">{{ $sticker->phone }}</div>
            </div>
            <div class="safety-tips">
              <ul class="featureLinks">
                <li>Şəhər <span class="right">{{$sticker->city->name}}</span></li>
                <li>Kateqoriya <span class="right">{{$sticker->sub_category->category->name}}</span></li>
                <li> Yeni<span class="right">{{ ($sticker->new == 1) ? 'Bəli' : 'Xeyr'}}</span></li>
              </ul>
            </div>
            <br>
            <div class="row">
              @if(Auth::check() && Auth::user()->id == $sticker->user_id)
                <div class="col-md-3">
                  <a href="{{ url('/sticker').'/'.$sticker->id.'/edit' }}" class="btn btn-warning">Düzəliş et</a>
                </div>
                <div class="col-md-3">
                  <form class="form-group" id='data-item-form{{$sticker->id}}' action="{{ url('/delete').'/'.$sticker->id }}" method="post">
                    @csrf
                    <a href="javascript:void(0)" onclick="removeItem({{ $sticker->id }})" class="btn btn-danger">Sil</a>
                  </form>
                </div>
                <div class="col-md-6">
                  <?php
                  $wishlist = App\Wishlist::where('user_id', Auth::user()->id)
                  ->where('sticker_id',$sticker->id)->first();
                  ?>
                  <form action="{{route('wishlist.store')}}" id="wishlist_form" method="post">
                    {{csrf_field()}}
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}" />
                    <input name="sticker_id" type="hidden" value="{{$sticker->id}}" />
                    <button type="submit" id="wishlist" class="form-control">
                      @if($wishlist) Seçilmişlərdən çıxart @else Seçilmişlərə əlavə et @endif
                    </button>
                  </form>
                </div>
                <div class="col-md-12">
                    <div id="wishlistResponse">

                    </div>

                </div>
              @endif
            </div>
          @else
            <h3 class="alert alert-danger">Elan aktiv deyil</h3>
          @endif

        </div>
      </div>
    </div>
  </div>
  <!--Detail page end-->
@endsection
@section('js')
  <script src="{{ asset('backend/js/sweet-alert.js') }}"></script>
  <script>
  function removeItem($id) {

    swal({
      title: "Are you sure",
      text: "You will be able to recover this data!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, sure",
      cancelButtonText: "No, cancel",
      allowOutsideClick: true,
    },
    function (isConfirm) {
      if (isConfirm) {
        $('#data-item-form' + $id).submit();
      }
    });
  }

  $("#wishlist_form").submit(function(){

    $.ajax({
      url: '{{route('wishlist.store')}}',
      dataType: 'json',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      method:"POST",
      data:$(this).serialize(),
      success: function (data) {
        if(data=='added'){
          $("#wishlistResponse").empty().html('<div class="alert alert-success">Elan seçilmişlərə əlavə olundu</div>');
          $("#wishlist").text("Seçilmişlərdən çıxart");
        }else{
          $("#wishlistResponse").empty().html('<div class="alert alert-success">Elan seçilmişlərdən silindi</div>');
          $("#wishlist").text("Seçilmişlərə əlavə et");
        }
      }
    });
    return false;
  });


  </script>
  <script defer src="{{ asset('frontend/classified/js/jquery.flexslider.js') }}"></script>
  <script type="text/javascript">
  $(window).load(function(){
    $('.flexslider').flexslider({
      animation: "slide",
      controlNav: "thumbnails",
      start: function(slider){
        $('body').removeClass('loading');
      }
    });
  });
  </script>
@endsection
