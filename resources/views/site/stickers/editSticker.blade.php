@extends('site.layouts.app')
  @section('css')
    <link rel="stylesheet" type="text/css"  href="{{ asset('css/dropzone.css') }}">
  @endsection
@section('content')
  <!--inner heading start-->
  <div class="inner-heading">
    <div class="container">
      <h1>Elan düzəlişi</h1>
    </div>
  </div>
  <!--inner heading end-->

  <!--ad post start-->
  <div class="inner-wrap listing">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-2"></div>
        <div class="col-md-6 col-sm-8">
          <div class="login">
            <div class="contctxt">İnformasiyaları əlavə edin</div>
            <div class="formint conForm">
              <div id="errorResponse">

              </div>
              <form class="form-group" action="" id="editSticker" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <select class="form-control" name="category" id="category">
                        <?php foreach ($categories as $key => $category): ?>
                          @if ($category->id == $sticker->sub_category->category->id )
                            <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                          @else
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                          @endif
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <select class="form-control" name="sub_category_id" id="sub_category">
                        <option value="{{ $sticker->sub_category->id }}" selected>{{ $sticker->sub_category->name }}</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="input-wrap">
                  <input type="text" name="name" class="form-control" placeholder="Elanın adı" value="{{$sticker->name}}">
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <input type="text" name="price" placeholder="Qiymət" class="form-control" value="{{$sticker->price}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap row">
                      <label for="checkbox" class="col-md-6"><h3>Yeni</h3></label>
                      <input type="checkbox" name="new" id="checkbox" @if($sticker->new == 1) checked @else '' @endif class="form-control col-md-6">
                    </div>
                  </div>
                </div>
                <div class="input-wrap">
                  <textarea name="description" rows="5" cols="80" class="form-control" placeholder="Məzmun">{!! $sticker->description !!}</textarea>
                </div>
                <div class="input-wrap">
                  <div id="myDropZone" class="dropzone dropzone-design">
                    <div class="dz-default dz-message"><span>Şəkilləri bura yerləşdirin</span></div>
                  </div>
                  <div id="productImages"></div><br>

                  <div class="row">
                    <div class="col-md-4 col-md-offset-4 check_del">
                      <button type="button" onclick="deleteImages();"class="form-control btn-danger text-white">Seçilmişləri Sil</button>
                    </div>
                  </div>
                  <div class="row">

                    @if(!empty($sticker->images))
                      @foreach ($sticker->images as $image)
                        <div class="col-md-3 pd-10 sticker_img" id="img_{{$image->id}}">
                          <input type="checkbox" id="c_{{$image->id}}"  class="checkBoxes" value="{{$image->id}}">
                          <label for="c_{{$image->id}}">Sil</label>
                          <img class="img-responsive" style="width:100px" src="{{ asset($image->file_name) }}" alt="image"/>
                          <button type="button" onclick="deleteImage({{$image->id}});" class="del-image"><i class="fa fa-times"></i></button>
                        </div>
                      @endforeach
                    @endif

                  </div>
                </div>

                <div class="contctxt persional">Şəxsi İnformasiyalar</div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <input type="text" name="author_name" class="form-control"
                      value="{{ (Auth::check()) ? Auth::user()->name : '' }}" placeholder="Adınız">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <input type="text" name="author_email" class="form-control"
                      value="{{ (Auth::check()) ? Auth::user()->email : '' }}" placeholder="Emailiniz">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <input type="text" name="phone" placeholder="Mobil nömrə" value="{{ $sticker->phone }}" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-wrap">
                      <select class="form-control" name="city_id">
                        <?php foreach ($cities as $key => $city): ?>
                          @if ($city->id == $sticker->city->id )
                            <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                          @else
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                          @endif
                        <?php endforeach; ?>
                      </select>
                  </div>
                </div>

                <div class="sub-btn">
                  <button type="submit" id="editSticker_submit" class="sbutn">Düzəliş et</button>
                </div>
              </form>
            </div>

          </div>
        </div>
        <div class="col-md-3 col-sm-2"></div>
      </div>
    </div>
  </div>
  <!--ad post end-->


@endsection

@section('js')
  <script type="text/javascript" src="{{ asset('/js/dropzone.js') }}"></script>
  <script>

  Dropzone.autoDiscover = false;

  $(function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("div#myDropZone").dropzone({
      url: "{{ url('/') }}/sticker/addImage",
      sending: function(file, xhr, formData) {
        formData.append("_token", CSRF_TOKEN);
      },
      success:function(re){
        var obj = $.parseJSON(re.xhr.response);
        $("#productImages").append('<input type="hidden" value="'+ obj.file_name +'" name="photos[]"/>');
      }
    });
  });


function deleteImage(image_id){
   if (confirm('Silinsin?')){
     $.ajax({
       url:'{{url('/stickers/deleteImage').'/'}}' + image_id,
       method:'GET',
       success:function(response){
         if(response == "success"){
           $("#img_"+image_id).remove();
         }else{
           alert("Səhv! Şəkil silinmədi!")
         }
       }
     });
   }
}

$("#category").change(function(){
  $("#sub_category").empty().append('<option value="">Seçin</option>');
  $.ajax({
    type: 'GET',
    url: '{{ url('/') }}/sticker/add/getSubCategory/'+$(this).val(),
    dataType: 'json',
    success: function (data) {
      $.each(data, function(index, element) {
        $('#sub_category').append('"<option value="'+element.id+'">' + element.name +'</option>');
      });
    }
  });
});

$("#editSticker").submit(function(){
  $("#editSticker_submit").text("Düzəliş edilir...").css('opacity','0.6');
  $.ajax({
    url:"{{url('/sticker').'/'.$sticker->id.'/edit'}}",
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    method:"POST",
    data:$(this).serialize(),
    success: function(response){
       $('html, body').animate({scrollTop:$('#errorResponse').offset().top}, 'slow');
      if(response.message == "success"){
        $("#errorResponse").empty().html('<div class="alert alert-success">Düzəliş edildi</div>');
        $(".form-control").val("");
        $("#checkbox").prop("checked", false);
        Dropzone.forElement('#myDropZone').removeAllFiles(true)
      }else{
        $("#errorResponse").empty();
        $("#errorResponse").addClass('alert alert-danger');
        $.each(response.message, function( k, v ) {
            $("#errorResponse").append('<span>'+v+'</span><br>');
          });
      }
      $("#editSticker_submit").text("Düzəliş et").css('opacity','1');
    },
    error: function(response){
      $("#errorResponse").empty().html('<div class="alert alert-danger">Səhv baş verdi</div>');
      $("#editSticker_submit").text("Əlavə et").css('opacity','1');
    }
  });

  return false;
});




function deleteImages(){
  var images = [];
  $(".checkBoxes").each(function(){
    if(this.checked){
      var image_id= $(this).val();
      images.push(image_id);
    }
  });

  if(images === undefined || images.length == 0){
    alert("Zəhmət olmasa seçin");
  }else{
    if (confirm('Silinsin?')){
      $.ajax({
        url:'{{ url('/delete/images')}}',
        method:'DELETE',
        data: {img_id: images,
          "_token": "{{ csrf_token() }}"
        },
        success:function(response){
          if(response == "success"){
            $.each(images, function( index, value ) {
              $("#img_"+value).remove();
            });
          }else{
            alert("Silinmədi!")
          }
        }
      });
    } //confirm end
  }
}

</script>
@endsection
