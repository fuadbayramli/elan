@extends('site.layouts.app')
@section('content')

  <!--inner heading start-->
  <div class="inner-heading">
    <div class="container">
      <h1>Haqqımızda</h1>
    </div>
  </div>
  <!--inner heading end-->

  <!-- about start-->
  <div class="inner-wrap about">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="aboutImg"><img src="{{asset('frontend/classified/images/about.png') }}" alt="img"></div>
        </div>
        <div class="col-md-8">
          <h1>Tap.Az saytında reklam</h1>
          <p>TAP.AZ - Azərbaycanın ən məşhur və stabil artan internet resurslarından biridir. Hər gün bu saytı minlərlə insan ziyarət edir.

TAP.AZ saytında reklam sizin brendləriniz, məhsullarınız, xidmətləriniz, aksiyalarınız və əhəmiyyətli hadisələriniz barədə məlumatı çox geniş auditoriyaya çatdırmaq üçün ən gözəl üsüllarından biridir!
Siz həm brendinizin tanınmasının əhəmiyyətli dərəcədə artmasına, həm də məhsullarınız barədə məlumatın geniş yayılmasına nail olacaq, və həmçinin şirkətlərinizin internet saytlarının ziyarət edilməsinin sadə üsülla çoxalması imkanını əldə edəcəksiniz.
<br/>
            <br/>
            Sizinlə əməkdaşlığa şad olardıq!  </p>
            <p>+99412 599 08 07 <br>
                +99455 206 31 01 <br>
                info@digit.az</p>
        </div>
      </div>
      
    </div>
  </div>
  <!--about end -->


  @endsection
