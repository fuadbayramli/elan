@extends('site.layouts.app')
@section('content')
  <!--inner start-->
  <div class="inner-heading">
    <div class="container">
      <h1>Bizimlə əlaqə</h1>
    </div>
  </div>
  <!--inner end-->

  @php
    $site_informations = \Cache::remember('site_informations', 86400, function () {
      return App\SiteInformation::get();
    });
  @endphp

  <div class="inner-wrap">
    <div class="container">
      <!--contact start-->
      <div class="contact-info">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="contactWrp">
              <div class="contact-icon"> <i class="fa fa-map-marker" aria-hidden="true"></i> </div>
              <h5>Bizim Ünvan</h5>
              <p>{{$site_informations->first()->address }}</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="contactWrp">
              <div class="contact-icon"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
              <h5>Bizimlə Əlaqə</h5>
              <p>Telefon : {{$site_informations->first()->main_number }}<br>
                Fax : {{$site_informations->first()->main_number }}</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="contactWrp">
              <div class="contact-icon"> <i class="fa fa-laptop" aria-hidden="true"></i> </div>
              <h5> Onlayn Əlaqə</h5>
              <p>Email : {{$site_informations->first()->main_email}}<br>
                Email : {{$site_informations->first()->main_email}}</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="contactWrp">
              <div class="contact-icon"> <i class="fa fa-globe" aria-hidden="true"></i> </div>
              <h5>Sayt</h5>
              <p>Sayt : {{$site_informations->first()->link}}<br>
                Sayt : {{$site_informations->first()->link}}</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div id="contactResponse">

          </div>
          {!! Form::open(['method'=>'POST', 'class'=>'sn-form contact-form', 'id' => 'contact-form', 'action'=> 'MessageController@store', 'files'=>true]) !!}
            <div class="row">
              <div class="col-sm-6">
                <div class="input-wrap">
                  {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => "Tam Ad"])!!}
                  <div class="form-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="input-wrap">
                  {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => "Mobil Nömrə"])!!}
                  <div class="form-icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                </div>
              </div>
            </div>
            <div class="input-wrap">
              {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => "Email"])!!}
              <div class="form-icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
            </div>
            <div class="input-wrap">
              {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => "Mesaj..."])!!}
              <div class="form-icon"><i class="fa fa-comment" aria-hidden="true"></i></div>
            </div>
            <div class="contact-btn">
              <button class="sub" id="contact_submit" type="submit" value="submit" name="submitted"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Göndər</button>
            </div>
          {!! Form::close() !!}
        </div>
        <div class="col-md-6">
        <div class="mapWrp">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3439665.415133291!2d-85.42187768895461!3d32.658159955276645!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88f136c51d5f8157%3A0x6684bc10ec4f10e7!2sGeorgia!5e0!3m2!1sen!2sus!4v1505905563776" width="100" height="300" style="border:0" allowfullscreen></iframe>
        </div>
        </div>
      </div>
      <!--contact end-->
@endsection

@section('js')
  <script type="text/javascript">
  $(document).ready(function(){
    $("#contact-form").submit(function(){

      $("#contact_submit").text("Göndərilir...").css('opacity','0.6');

      $.ajax({
        url:"{{url('message')}}",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"POST",
        data:$(this).serialize(),
        success: function(response){
          $('html, body').animate({scrollTop:$('#contactResponse').offset().top}, 'slow');
          if(response.message == "success"){
            if ($(".alert")[0]){
              $("#contactResponse").removeClass();
            }
            $("#contactResponse").empty().html('<div class="alert alert-success">Göndərildi</div>');
            $(".form-control").val("");
          }else{
            $("#contactResponse").empty();
            $("#contactResponse").addClass('alert alert-danger');
            $.each(response.message, function( k, v ) {
                $("#contactResponse").append('<span>'+v+'</span><br>');
              });
          }
          $("#contact_submit").text("Göndər").css('opacity','1');
        },
        error: function(response){
          $("#contactResponse").empty().html('<div class="alert alert-danger">Səhv baş verdi</div>');
          $("#contact_submit").text("Göndər").css('opacity','1');
        }
      });

      return false;

    });
  });
  </script>
@endsection
