@extends('site.layouts.app')
@section('content')

  <div class="inner-heading">
    <div class="container">
      <h1>Seçilmişlər</h1>
    </div>
  </div>

  <div class="inner-wrap about">
    <div class="container">
        @if(count($wishlists)!=0)
          <ul class="row feature-service" id="row">
            @foreach($wishlists as $wishlist)
              @if ($wishlist->sticker != NULL)
                <li class="col-md-3 col-sm-6 col-xs-12" id="f_{{ $wishlist->sticker->id }}">
                  <div class="feature-image"><img src="{{ asset($wishlist->sticker->img)}}" alt="img">
                    <div class="price">{{ $wishlist->sticker->price}} Azn</div>
                  </div>
                  <div class="feature">
                    <div class="feat-bg">
                      <h3><a href="{{url('sticker').'/'.$wishlist->sticker->id}}">{{ str_limit($wishlist->sticker->name, 20) }}</a></h3>
                    </div>
                    <div class="feature-detail">
                      <ul class="featureList">
                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{$wishlist->sticker->city->name}}</li>
                        <li><i class="fa fa-clock-o" aria-hidden="true"></i> {{$wishlist->sticker->updated_at->format('d M Y')}}</li>
                      </ul>
                    </div>
                  </div>
                </li>
              @endif
            @endforeach
          </ul>
          {{$wishlists}}
        @else
          <h3 class="alert alert-danger">
            Elan tapılmadı
          </h3>
        @endif
    </div>
  </div>

@endsection
@section('js')

@endsection
