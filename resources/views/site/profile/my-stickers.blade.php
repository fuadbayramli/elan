@extends('site.layouts.app')
@section('content')

  <div class="inner-heading">
    <div class="container">
      <h1>Mənim elanlarım</h1>
    </div>
  </div>

  <div class="inner-wrap about">
    <div class="container">
      <div class="row">
        <div class="col-md-2" id="sort">
          <select id="sort_select" class="form-control" name="sort">
            <option value="">Sırala</option>
            <option value="0">Son elanlar</option>
            <option value="1">Qiymət (azdan çoxa)</option>
            <option value="2">Qiymət (çoxdan aza)</option>
          </select>
        </div>
      </div>
        @if(count($stickers)!=0)
          <ul class="row feature-service" id="row">
            @foreach($stickers as $sticker)
              @include('site.stickers.includes.sticker')
            @endforeach
          </ul>
          <div class="view-btn"><button type="submit" id="more"><a>Daha çox</a></button></div>
        @else
          <h3 class="alert alert-danger">
            Elan tapılmadı
          </h3>
        @endif
    </div>
  </div>

@endsection
@section('js')
<script>

var page_number =2
$("#more").click(function(){
  $("#more a").text('Yüklənir...');
  $.ajax({
    type: 'GET',
    url: '/elan/my-stickers/?page='+ page_number+'&sort='+$("#sort_select").val(),
    success: function (response) {
      $("#row").append(response);
      page_number+=1;
      $("#more a").text('Daha çox');
      if(response==""){
        $("#more a").hide();
      }
    }
  });
});


$("#sort_select").change(function(){
  page_number=1;
  $("#row").empty();
  $("#more a").show();
  $("#more a").text('Yüklənir...');
  $.ajax({
    type: 'GET',
    url: '/elan/my-stickers/?page='+ page_number+'&sort='+$("#sort_select").val(),
    success: function (response) {
      $("#row").append(response);
      page_number+=1;
      $("#more a").text('Daha çox');
      if(response==""){
         $("#more a").hide();
      }

    }
  });

});
</script>
@endsection
