@extends('site.layouts.app')

@section('content')

  <!--inner heading start-->
  <div class="inner-heading">
    <div class="container">
      <h1>Register</h1>
    </div>
  </div>
  <!--inner heading end-->



  <!--login start-->
  <div class="inner-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-2"></div>
        <div class="col-md-6 col-sm-8">
          <div class="login">
            <div class="contctxt">Please complete all fields.</div>
            <div class="formint conForm">
              <form method="POST" action="{{ route('register') }}">
                  @csrf
                <div class="input-wrap">
                  <input id="name" type="text" placeholder="Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                  @if ($errors->has('name'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="input-wrap">
                  <input id="email" type="email" placeholder="E-Mail Address" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="input-wrap">
                  <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="input-wrap">
                  <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required>
                </div>
                <div class="sub-btn">
                  <button type="submit" class="sbutn">Register</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-2"></div>
      </div>
    </div>
  </div>
  <!--login end-->



@endsection
