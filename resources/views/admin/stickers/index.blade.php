@extends('admin.layouts.app')
@section('customcss')
  <link href="{{ asset('backend/css/sweet-alert.css') }}" rel="stylesheet">
@endsection
@section('content')

  <!-- Content Wrapper START -->
  <div class="page-container">
    <div class="main-content">
      <div class="container-fluid">
        <div class="page-header">
          <h2 class="header-title">Stickers</h2>

          <div class="app header-success-gradient">
            <div class="layout">
              <div class="header navbar no-fixed">
                <div class="header-container">
                  <ul class="nav-left">
                    <li class="search-box">
                      <a class="search-toggle" href="javascript:void(0);">
                        <i class="search-icon mdi mdi-magnify"></i>
                        <i class="search-icon-close mdi mdi-close-circle-outline"></i>
                      </a>
                    </li>
                    <li class="search-input">
                      <form method="get">
                        <input class="form-control" type="text" id="search" name="q" placeholder="Axtarın...">
                        <button class="btn btn-default d-none" type="submit"></button>
                      </form>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <br>
        </div>
        <div class="card">
          <div class="card-body">
            <div class="form-group col-md-3">
              <button type="button" onclick="deleteStickers();"class="form-control btn-gradient-danger text-white">Seçilmişləri Sil</button>
            </div>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>
                    <div class="col-md-9 checkbox">
                      <input type="checkbox" id="options" name="checkbox" class="form-control">
                      <label for="options"></label>
                    </div>
                  </th>
                  <th scope="col">#</th>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Author Name</th>
                  <th>User</th>
                  <th>Price</th>
                  <th>Category</th>
                  <th>City</th>
                  <th>Status</th>
                  <th>Created at</th>
                  <th>Updated at</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @if($stickers)
                  @foreach($stickers as $key => $sticker)
                    <tr id="tr_{{$sticker->id}}">
                      <td>
                        <div class="col-md-9 checkbox">
                          <input type="checkbox" id="c_{{$sticker->id}}"  class="form-control checkBoxes" value="{{$sticker->id}}">
                          <label for="c_{{$sticker->id}}"></label>
                        </div>
                      </td>
                      <th scope="row">{{$sticker->id}}</th>
                      <td><img src="{{ asset($sticker->img) }}" style="height: 40px;" alt=""></td>
                      <td><a href="{{ url('/sticker').'/'.$sticker->id }}" target="_blank">{{$sticker->name}}</a></td>
                      <td>{{$sticker->author_name}}</td>
                      <td><a href="{{ !empty($sticker->user->name) ? url('/user').'/'.$sticker->user->id.'/stickers' : url('#') }}">
                        {{$sticker->user->name ?? '-'}}
                      </a>
                    </td>
                    <td>{{$sticker->price}}</td>
                    <td><a href="{{url('/').'/'.$sticker->sub_category->category->slug.'/'.$sticker->sub_category->slug.'/stickers'}}">
                      {{ $sticker->sub_category->category->name }}/{{ $sticker->sub_category->name }}
                    </a>
                  </td>
                  <td><a href="{{ url('/city').'/'.$sticker->city->id.'/stickers' }}">{{ $sticker->city->name }}</a></td>
                  <td><button class="btn @if($sticker->status==1)
                    btn-gradient-success
                  @elseif($sticker->status==2)
                    btn-gradient-warning
                  @else btn-gradient-danger @endif" id="status_{{$sticker->id}}" onclick="changeStatus({{ $sticker->id }})">
                    @if($sticker->status==1) Aktiv @elseif($sticker->status==2) Təsdiq gözləyir @else Deaktiv @endif
                    </button>
                  </td>
                  <td>{{$sticker->created_at->diffForhumans()}}</td>
                  <td>{{$sticker->updated_at->diffForhumans()}}</td>
                  <td><a href="{{ route('admin.stickers.edit', $sticker->id) }}"><i class="fa fa-eye"></i></a>
                    {!! Form::open(['method'=>'DELETE', 'action'=> ['Admin\AdminStickerController@destroy', $sticker->id],'id' => 'data-item-form'.$sticker->id]) !!}
                    <div class="form-group">
                      <a href="javascript:void(0)" onclick="removeItem({{ $sticker->id }})"><i class="fa fa-trash"></i></a>
                    </div>
                    {!! Form::close() !!}
                  </td>
                </tr>
              @endforeach

            @endif

          </tbody>
        </table>
        <div class="row">
          <div class="col-sm-6">
            @if($stickers->total() == 0)
               0
            @else
              {{($stickers->currentpage()-1)*$stickers->perpage()+1}}
            @endif
             to {{(($stickers->currentpage()-1)*$stickers->perpage())+$stickers->count()}} of {{$stickers->total()}}
          </div>
          <div class="col-sm-6">
            {{$stickers->render()}}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Content Wrapper END -->

@endsection


@section('customjs')
  <script src="{{ asset('backend/js/sweet-alert.js') }}"></script>
  <script>
  function removeItem($id) {
    swal({
      title: "Are you sure",
      text: "You will be able to recover this data!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, sure",
      cancelButtonText: "No, cancel",
      allowOutsideClick: true
    },
    function (isConfirm) {
      if (isConfirm) {
        $('#data-item-form' + $id).submit();
      }
    });
  }
  </script>

  <script>
  $(document).ready(function(){
    $("#options").click(function(){
      if(this.checked){
        $(".checkBoxes").each(function(){
          this.checked = true;
        });
      }else{
        $(".checkBoxes").each(function(){
          this.checked = false;
        });
      }
    })
  })

  function deleteStickers(){
    var stickers = [];
    $(".checkBoxes").each(function(){
      if(this.checked){
        var sticker_id= $(this).val();
        stickers.push(sticker_id);
      }
    });

    if(stickers === undefined || stickers.length == 0){
      alert("Zəhmət olmasa seçin");
    }else{
      if (confirm('Silinsin?')){
        $.ajax({
          url:'{{ url('/delete/stickers')}}',
          method:'POST',
          data: {s_id: stickers,
            "_token": "{{ csrf_token() }}"
          },
          success:function(response){
            if(response == "success"){
              $.each(stickers, function( index, value ) {
                $("#tr_"+value).fadeOut();
              });
            }else{
              alert("Silinmədi!")
            }
          }
        });
      } //confirm end
    }
  }
  </script>
  <script>
  function changeStatus(id){
    $.ajax({
      type: 'GET',
      url: '{{ url('/admin/stickers/status') }}/'+id,
      dataType: 'json',
      success: function (data) {
        if (data.status == 1) {
          $('#status_'+data.id).text(
            'Aktiv'
          ).attr('class', 'btn btn-success');
        }else {
          $('#status_'+data.id).text(
            'Deaktiv'
          ).attr('class', 'btn btn-danger');
        };
      }
    });
  }
  </script>
@endsection
