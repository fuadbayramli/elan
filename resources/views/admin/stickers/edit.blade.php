@extends('admin.layouts.app')
@section('customcss')
  <link rel="stylesheet" type="text/css"  href="{{ asset('css/dropzone.css') }}">
@endsection
@section('content')

    <div class="page-container">
        <div class="main-content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header border bottom">
                        <h4 class="card-title">Edit Sticker</h4>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal form-label-left" action="{{ url('/admin/stickers').'/'.$sticker->id.'/update' }}" method="post" enctype="multipart/form-data">
                          @csrf
                        <div class="row">
                            <div class="col-sm-8 offset-sm-2">
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="category" class="control-label">Kateqoriyalar:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <select class="form-control" name="category" id="category">
                                      <?php foreach ($categories as $key => $category): ?>
                                        @if ($category->id == $sticker->sub_category->category->id )
                                          <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                        @else
                                          <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endif
                                      <?php endforeach; ?>
                                    </select>
                                    @if(!empty($errors->get('category')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('category') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="sub_category_id" class="control-label">Alt kateqoriyalar:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <select class="form-control" name="sub_category_id" id="sub_category">
                                        <option value="{{ $sticker->sub_category->id }}" selected>{{ $sticker->sub_category->name }}</option>
                                    </select>
                                    @if(!empty($errors->get('sub_category_id')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('sub_category_id') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="name" class="control-label">Elanın adı:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <input type="text" name="name" class="form-control" value="{{ $sticker->name }}">
                                    @if(!empty($errors->get('name')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('name') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="city_id" class="control-label">Şəhərlər:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <select class="form-control" name="city_id">
                                      <?php foreach ($cities as $key => $city): ?>
                                        @if ($city->id == $sticker->city->id )
                                          <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                        @else
                                          <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endif
                                      <?php endforeach; ?>
                                    </select>
                                    @if(!empty($errors->get('city_id')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('city_id') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="price" class="control-label">Qiymət:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <input type="text" name="price" class="form-control" value="{{ $sticker->price }}">
                                    @if(!empty($errors->get('price')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('price') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="new" class="control-label">Yeni:</label>
                                  </div>
                                  <div class="col-md-9 checkbox">
                                      <input type="checkbox" id="checkbox_new" name="new" class="form-control" @if($sticker->new == 1) checked @else '' @endif>
                                      <label for="checkbox_new">Bəli</label>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="description" class="control-label">Məzmun:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <textarea name="description" rows="5" cols="80" class="form-control" id="editor1">{{$sticker->description}}</textarea>
                                    @if(!empty($errors->get('description')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('description') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="new" class="control-label">Şəkillər:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <div id="myDropZone" class="dropzone dropzone-design">
                                      <div class="dz-default dz-message"><span>Şəkilləri bura yerləşdirin</span></div>
                                    </div>
                                    <div id="productImages"></div>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="author_name" class="control-label">Adınız:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <input type="text" name="author_name" class="form-control"
                                    value="{{ (Auth::check()) ? Auth::user()->name : '' }}">
                                    @if(!empty($errors->get('author_name')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('author_name') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="author_email" class="control-label">Emailiniz:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <input type="text" name="author_email" class="form-control"
                                    value="{{ (Auth::check()) ? Auth::user()->email : '' }}">
                                    @if(!empty($errors->get('author_email')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('author_email') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="phone" class="control-label">Mobil nömrə:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <input type="text" name="phone" class="form-control" value="{{ $sticker->phone }}">
                                    @if(!empty($errors->get('phone')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('phone') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="status" class="control-label">Status:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <select class="form-control" name="status" id="status">
                                        <option value="0" @if($sticker->status == 0) selected @endif>Deactive</option>
                                        <option value="1" @if($sticker->status == 1) selected @endif>Active</option>
                                    </select>
                                    @if(!empty($errors->get('status')))
                                      <ul class="alert-danger">
                                        @foreach ($errors->get('status') as $error)
                                          <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="img" class="control-label">Şəkillər:</label>
                                  </div>
                                  <div class="col-md-9 row">
                                    @if(!empty($sticker->images))
                                      @foreach ($sticker->images as $image)
                                        @if($sticker->img == $image->file_name)
                                          @php $radio_check='checked'; @endphp
                                        @else
                                          @php $radio_check=NULL; @endphp
                                        @endif

                                        <div class="col-md-4 sticker_img" id="img_{{$image->id}}">
                                          <div class="radio">
                                                <input id="radio_{{$image->id}}" name="img" {{$radio_check}} type="radio" value="{{$image->file_name}}"/>
                                                <label for="radio_{{$image->id}}">Cover</label>
                                          </div>

                                          <img class="img-responsive" style="width:125px" id="img__{{$image->id}}" src="{{ asset($image->file_name). '?' . time()}}" alt="image"/>
                                          <button type="button" onclick="deleteImage({{$image->id}});" class="del-image"><i class="fa fa-trash"></i></button>
                                          <div class="">
                                            <button type="button" onclick="rotateLeft('{{$image->file_name}}',{{$image->id}},'{{$image->file_name}}')" class="btn btn-warning" name="button"><i class="fa fa-undo"></i></button>
                                            <button type="button" onclick="rotateRight('{{$image->file_name}}',{{$image->id}},'{{$image->file_name}}')" class="btn btn-warning" name="button"><i class="fa fa-undo fa-flip-horizontal"></i></button>
                                          </div>
                                        </div>

                                      @endforeach
                                    @endif
                                  </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-sm-center">
                                        {!! Form::submit('Edit', ['class'=>'btn btn-gradient-success', 'id' => 'send']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </form>
                </div>


            </div>
        </div>
    </div>
    </div>
    </div>

@endsection


@section('customjs')
  <script src="{{ asset('backend/ckeditor/ckeditor.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/dropzone.js') }}"></script>
  <script>
      CKEDITOR.replace( 'editor1' );
  </script>
  <script>

  Dropzone.autoDiscover = false;

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $("div#myDropZone").dropzone({
      url: "{{ url('/admin/sticker/addImage') }}",
      sending: function(file, xhr, formData) {
        formData.append("_token", CSRF_TOKEN);
      },
      success:function(re){
        var obj = $.parseJSON(re.xhr.response);
        $("#productImages").append('<input type="hidden" value="'+ obj.file_name +'" name="photos[]"/>');
      }
    });

  function deleteImage(image_id){
     if (confirm('Silinsin?')){
       $.ajax({
         url:'{{ url('/admin/sticker/deleteImage')}}/' + image_id,
         method:'GET',
         success:function(response){
           if(response == "success"){
             $("#img_"+image_id).remove();
           }else{
             alert("Səhv! Şəkil silinmədi!")
           }
         }
       });
     }
  }

  $("#category").change(function(){
    $("#sub_category").empty().append('<option value="">Seçin</option>');
    $.ajax({
      type: 'GET',
      url: '{{ url('/') }}/sticker/add/getSubCategory/'+$(this).val(),
      dataType: 'json',
      success: function (data) {
        $.each(data, function(index, element) {
          $('#sub_category').append('"<option value="'+element.id+'">' + element.name +'</option>');
        });
      }
    });
  });




  function rotateLeft(file,id,image){
          var now = $.now();
          $.ajax({
            url: "{{route('productImageRotateLeft')}}",
            type: 'POST',
            data: {file:file},
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              $("#img__"+id).attr("src",'{{asset('/')}}'+image+"?"+now)
            }
          });

        }

        function rotateRight(file,id,image){
          var now = $.now();
          $.ajax({
            url: "{{route('productImageRotateRight')}}",
            type: 'POST',
            data: {file:file},
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              $("#img__"+id).attr("src",'{{asset('/')}}'+image+"?"+now)
            }
          });

        }

  </script>
@endsection
