@extends('admin.layouts.app')
@section('customcss')
    <link href="{{ asset('backend/css/sweet-alert.css') }}" rel="stylesheet">
@endsection
@section('content')

  <!-- Content Wrapper START -->
  <div class="page-container">
    <div class="main-content">
      <div class="container-fluid">
        <div class="page-header">
          <h2 class="header-title">Messages</h2>
          <div class="app header-success-gradient">
            <div class="layout">
              <div class="header navbar no-fixed">
                <div class="header-container">
                  <ul class="nav-left">
                    <li class="search-box">
                      <a class="search-toggle" href="javascript:void(0);">
                        <i class="search-icon mdi mdi-magnify"></i>
                        <i class="search-icon-close mdi mdi-close-circle-outline"></i>
                      </a>
                    </li>
                    <li class="search-input">
                      <form method="get">
                        <input class="form-control" type="text" name="q" placeholder="Axtarın...">
                        <button class="btn btn-default d-none" type="submit"></button>
                      </form>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <br>
        </div>
        <div class="card">
          <div class="card-body">
            <div class="form-group col-md-3">
              <button type="button" onclick="deleteMessage();"class="form-control btn-gradient-danger text-white">Seçilmişləri Sil</button>
            </div>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>
                    <div class="col-md-9 checkbox">
                      <input type="checkbox" id="options" name="checkbox" class="form-control">
                      <label for="options"></label>
                    </div>
                  </th>
                  <th scope="col">#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Message</th>
                  <th>Created at</th>
                  <th>Updated at</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @if($messages)
                  @foreach($messages as $key => $messages_item)
                    <tr id="tr_{{$messages_item->id}}">
                      <td>
                        <div class="col-md-9 checkbox">
                          <input type="checkbox" id="c_{{$messages_item->id}}"  class="form-control checkBoxes" value="{{$messages_item->id}}">
                          <label for="c_{{$messages_item->id}}"></label>
                        </div>
                      </td>
                      <th scope="row">{{$messages_item->id}}</th>
                      <td>{{$messages_item->name}}</td>
                      <td>{{$messages_item->email}}</td>
                      <td>{{$messages_item->phone}}</td>
                      <td>{!!$messages_item->message!!}</td>
                      <td>{{$messages_item->created_at->diffForhumans()}}</td>
                      <td>{{$messages_item->updated_at->diffForhumans()}}</td>
                      <td>
                          {!! Form::open(['method'=>'DELETE', 'action'=> ['Admin\AdminMessageController@destroy', $messages_item->id],'id' => 'data-item-form'.$messages_item->id]) !!}
                          <div class="form-group">
                              <a href="javascript:void(0)" onclick="removeItem({{ $messages_item->id }})"><i class="fa fa-trash"></i></a>
                          </div>
                          {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach

                @endif

              </tbody>
            </table>

            <div class="row">
              <div class="col-sm-6">
                @if($messages->total() == 0)
                   0
                @else
                  {{($messages->currentpage()-1)*$messages->perpage()+1}}
                @endif
                 to {{(($messages->currentpage()-1)*$messages->perpage())+$messages->count()}} of {{$messages->total()}}
              </div>
              <div class="col-sm-6">
                {{$messages->render()}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Content Wrapper END -->

@endsection


@section('customjs')
  <script src="{{ asset('backend/js/sweet-alert.js') }}"></script>
  <script>
    $(document).ready(function(){
      $("#options").click(function(){
        if(this.checked){
          $(".checkBoxes").each(function(){
            this.checked = true;
          });
        }else{
          $(".checkBoxes").each(function(){
            this.checked = false;
          });
        }

      })
    })

    function deleteMessage(){
      var messages = [];

      $(".checkBoxes").each(function(){
        if(this.checked){
          var message_id= $(this).val();
          messages.push($(this).val());
        }
      });

      if(messages === undefined || messages.length == 0){
        alert("Zəhmət olmasa seçin");
      }else{

        if (confirm('Silinsin?')){

          $.ajax({
            url:'{{ url('/delete/messages')}}',
            method:'POST',
            data: {m_id: messages,
              "_token": "{{ csrf_token() }}"
            },
            success:function(response){
              if(response == "success"){
                $.each(messages, function( index, value ) {
                  $("#tr_"+value).fadeOut();
                });
              }else{
                alert("Silinmədi!")
              }
            }
          });

        } //confirm end
      }
    }
  </script>
  <script>
      function removeItem($id) {
          swal({
                  title: "Are you sure",
                  text: "You will be able to recover this data!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, sure",
                  cancelButtonText: "No, cancel",
                  allowOutsideClick: true
              },
              function (isConfirm) {
                  if (isConfirm) {
                      $('#data-item-form' + $id).submit();
                  }
              });
      }
  </script>
@endsection
